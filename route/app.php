<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// Route::get('think', function () {
//     return 'hello,ThinkPHP6!';
// });

// Route::get('hello/:name', 'index/hello');

// Route::rule('/')

Route::rule('/logout', 'index/logout');
Route::rule('/login', 'index/login');
Route::rule('/is_login', 'index/is_login');
Route::rule('/qywx_login', 'index/qywx_login');
Route::rule('/welcome', 'index/welcome');
// Route::rule('/test', 'index/test');
Route::rule('/no_permission', 'index/no_permission');
