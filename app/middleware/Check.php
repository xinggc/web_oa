<?php
declare (strict_types = 1);

namespace app\middleware;
use think\facade\Db;
class Check
{

    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $response = $next($request);
        $href = strtolower(join('/', [$request->controller(), $request->action()]));
        $arr_except = [
            'index/login',
            'index/logout',
            'index/qywx_login',
            'index/no_permission',
            'index/no_impower',
            'index/test',
            'callback/kuaishou',
            'callback/zijie',
            'callback/tencent',
            'callback/get_save_token',
            'callback/zj_third_party',
            'upload/index',
        ];
        $c = app('http')->getName();
        if (in_array($href, $arr_except) || $c == 'api'|| $c == 'test') {
            return $response;
        }
        
        // 需要登录
        if (empty(session('uid'))) {
            if (input('t') == 'json') {
                return json(['msg'=>'请重新登录', 'code'=>1]);
            } else {
                return redirect('/login');
            }
        }
        return $response;
    }

}
