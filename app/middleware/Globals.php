<?php
declare (strict_types = 1);

namespace app\middleware;
use app\model\Config;
use think\facade\View;

class Globals
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 页面meta信息
        $meta_data = Config::where(['group'=>'meta'])
            ->cache(true)->select()->toArray();
        $meta = [];
        foreach ($meta_data as $d) {
            $meta[$d['key']] = $d['value'];
        }
        View::assign('meta', $meta);
        return $next($request);
    }
    
    
    
}
