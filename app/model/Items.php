<?php
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;
class Items extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    //user 媒介
    public function media(){
        return $this->belongsTo(User::class, 'operate_id')->bind(['media_name'=>'username']);
    }
    //customer 客户
    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id')->bind(['customer_name'=>'name']);
    }
    //渠道
    public function channel(){
        return $this->belongsTo(Channel::class, 'channel_id')->bind(['channel_name'=>'name']);
    }
    //行业
    public function industry(){ 
        return $this->belongsTo('industry', 'industry_id')->bind(['industry_name'=>'name']); 
    }
    //账户
    public function account(){
        return $this->belongsTo(OpAccount::class, 'op_account_id')->bind(['account_name'=>'name','name_id']);
    }
    //获取项目列表
    public function get_items_list($p){
        
        if(isset($p['name'])&&!empty($p['name'])){
            $wh[] = ['name','like','%'.$p['name'].'%'];
        }else{
            $wh[] = ['id','>',0];
        }
        if(session('info.is_leader') != 1 && session('info.role_id') !=1 ){
            $wh[] = ['media_id','=',session('uid')];
        }
        $arr['list'] = $this->where($wh)->with(['media','channel','customer','industry','account'])->order('status desc,op_account_id desc,create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->count('id');
        foreach ($arr['list'] as $k=>$v){
            $arr['list'][$k]['yesterday_currency'] = ItemConsume::where('items_id',$v['id'])->whereTime('time', 'yesterday')->value('currency');
        }
        return $arr;
    }
    
    //添加项目
    public function items_add($data){
        $channel = new Channel();
        $validate = \think\facade\Validate::rule([
            'name|项目名称'  => 'require|max:50|unique:items',
            'customer_id|客户名称'  => 'require',
            'operate_type|运营类型'  => 'require',
            'op_account_id|账户ID'  => 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $qudao = $channel->get_acc_channel_info($data['op_account_id']);
            $data['operate_id'] = session('uid');
            $data['oper_name'] = session('info.username');
            $data['is_deliver'] = 1;
            $data['channel_id'] = $qudao['id'];
            $data['channel_rebate'] = $qudao['rebate'];
            $data['industry_id'] = OpAccount::where('id',$data['op_account_id'])->value('industry_id');
            
           
            $r = $this->save($data);
            if(!empty($r)){
                return true;
            }else{
                return false;
            }
            //return $this->save($data);
        }
    }
    
    
    
    
    
    /* 
     * 运营项目管理
     *  */
    //项目列表
    public function get_yy_item_list($p){
        if(empty($p['name']) && empty($p['op_account_id']) && empty($p['type'])){
            if(session('info.is_leader') != 1 && session('info.role_id') !=1 ){
                $acc_ids = $this->get_operate_accs();
                $acc_ids = implode(',', $acc_ids);
                $wh[] = ['i.op_account_id','in',$acc_ids.',0'];
            }
        }
        
        if(isset($p['name']) &&!empty($p['name'])){
            $wh[] = ['i.name','like','%'.$p['name'].'%'];
            $p['page'] = 1;
        }
        if(isset($p['op_account_id']) && !empty($p['op_account_id'])){
            $wh[] = ['i.op_account_id','=',$p['op_account_id']];
            $p['page'] = 1;
        }
        if(isset($p['type']) && !empty($p['type'])){
            $wh[] = ['i.type','=',$p['type']];
            $p['page'] = 1;
        }
        if(isset($p['operate_id']) && !empty($p['operate_id'])){
            $wh[] = ['i.operate_id','=',$p['operate_id']];
            $p['page'] = 1;
        }
        $wh[] = ['i.status','=',1];
        
       
        // $arr['list'] = $this->with(['media','channel','customer','industry','account'])->where($wh)->order('is_finish,op_account_id desc,create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['list'] = $this->alias('i')
        ->field('vii.*,h.name industry_name')
        ->join('view_items_info vii','i.id = vii.id','LEFT')
        ->join('industry h','i.industry_id = h.id','LEFT')
        ->where($wh)->order('i.is_finish,i.op_account_id desc,i.create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->alias('i')->where($wh)->count('i.id');
        foreach ($arr['list'] as $k=>$v){
           $arr['list'][$k]['yesterday_currency'] = ItemConsume::where('items_id',$v['id'])->whereTime('time', 'yesterday')->value('currency'); 
        }
        return $arr;
    }
    //获取当前运营下的所有账户
    function get_operate_accs(){
        return OpAccount::where('operate_id',session('uid'))->column('id');
    }
    //绑定账户
    public function get_bind_account($data){
        $validate = \think\facade\Validate::rule([
            'op_account_id|账户名称'  => 'require',
            'id|缺少参数'  => 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            
            return true;
        }
    }
    
    //账户做修改和状态改变时，判断该账户是否绑定了项目
    
    public function is_acc_item($id){
        $wh[] = ['op_account_id','=',$id];
        $wh[] = ['is_deliver','=',1];
        $wh[] = ['is_finish','=',0];
        $rs = $this->where($wh)->find();
        if($rs){
            return true;
        }else{
            return false;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}