<?php
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;
use app\model\Items;
class AccTransfer extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    //转账列表
    public function get_list($p){
        if(isset($p['acc_id']) && !empty($p['acc_id'])){
            $wh[] = ['at.out_acc','=',$p['acc_id']];
        }
        if(isset($p['media_id']) && !empty($p['media_id'])){
            $wh[] = ['at.media_id','=',$p['media_id']];
        }
        $wh[] = ['at.id','>',0];
        $arr['list'] = $this->alias('at')
        ->field('at.*,acc.name_id in_name_id,acc2.name_id out_name_id,u.username media_name,ch.name channel_name')
        ->join('op_account acc','at.in_acc = acc.id','Left')
        ->join('op_account acc2','at.out_acc = acc2.id','Left')
        ->join('user u','at.media_id = u.id','Left')
        ->join('channel ch','at.channel_id = ch.id','Left')
        ->where($wh)->order('status,create_time desc')->page($p['page'],$p['limit'])->select();
       
        $arr['cnt'] = $this->alias('at')->where($wh)->count('id');
        return $arr;
    }

    //添加转账
    public function get_add($data){
        $validate = \think\facade\Validate::rule([
            'out_acc|转出账户ID'  => 'require',
            'out_currency|转出账户币'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'in_acc|转入账户ID'  => 'require',
            'in_currency|转入账户币'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'rebate|客户返点'  => 'require|regex:/^-?[0-9]+(.[0-9]{1,3})?$/',
            'channel_rebate|端口返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'create_time|转账日期'  => 'require',
            'media_id|媒介'  => 'require',
            'transfer_url|转账凭证'  => 'require',
            'channel_id|端口'  => 'require',
            'customer_id|客户'  => 'require',
        ]);
    
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $s = $this->is_acc($data);
            if($s){
                return $s;//'转账账户，必须是在同一客户下，客户返点和端口返点必须一致';
            }
            $data['create_time'] = strtotime($data['create_time']);
            try {
                return $this->save($data);
            } catch (\Exception $e) {
                
                return $e->getMessage();
            }
        }
    }

    // 修改账户转账
    public function get_save($data){
        $validate = \think\facade\Validate::rule([
            'out_acc|转出账户ID'  => 'require',
            'out_currency|转出账户币'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'in_acc|转入账户ID'  => 'require',
            'in_currency|转入账户币'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'rebate|客户返点'  => 'require|regex:/^-?[0-9]+(.[0-9]{1,3})?$/',
            'channel_rebate|端口返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'create_time|转账日期'  => 'require',
            'media_id|媒介'  => 'require',
            'transfer_url|转账凭证'  => 'require',
            'channel_id|端口'  => 'require',
            'customer_id|客户'  => 'require',
            'id|请联系管理员'  => 'require',
        ]);
    
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $s = $this->is_acc($data);
            if($s){
                return $s;//'转账账户，必须是在同一客户下，客户返点和端口返点必须一致';
            }
            $data['create_time'] = strtotime($data['create_time']);
            $data['status'] = 1;
            try {
                return $this->strict(false)->save($data);
            } catch (\Exception $e) {
                
                return $e->getMessage();
            }
        }
    }
    //判断转账账户，是否是在同一客户下，客户返点和端口返点是否一致
    function is_acc($data){
        $w1[] = ['op_account_id','=',$data['in_acc']];
        $w1[] = ['is_finish','=',0];
        $acc1 = Items::where($w1)->find();
        $w2[] = ['op_account_id','=',$data['out_acc']];
        $acc2 = Recharge::where($w2)->order('create_time desc')->find();
       
        //判断是否是同一客户
        if(!empty($acc1) && !empty($acc2)){
            if($acc1['customer_id'] != $acc2['customer_id']){
                return '不是同一客户';
            }
        }else{
            return '客户不能为空';
        }
        //判断客户返点和端口返点是否一致
        $wh1[] = ['status','=',1];
        $wh1[] = ['op_account_id','=',$data['in_acc']];
        $wh1[] = ['customer_id','=',$acc2['customer_id']];
        $r1 = Recharge::where($wh1)->order('create_time desc')->find();
        $wh2[] = ['status','=',1];
        $wh2[] = ['op_account_id','=',$data['out_acc']];
        $wh2[] = ['customer_id','=',$acc2['customer_id']];
        $r2 = Recharge::where($wh2)->order('create_time desc')->find();
        if(empty($r2)){
            $wh3[] = ['in_acc','=',$data['out_acc']];
            $wh3[] = ['status','=',2];
            $r2 = $this->where($wh3)->order('create_time desc')->find();
        }
        if(!empty($r2)){
            if(!empty($r1)){
                if($r2['rebate'] != $r1['rebate']){
                    return '客户返点不同';//客户返点不同
                }
                if($r2['channel_rebate'] != $r1['channel_rebate']){
                    return '端口点位不同';//端口点位不同
                }
                if($r2['channel_id'] != $r1['channel_id']){
                    return '端口不同'; //端口不同
                }
            }
        }else{
            return '转出账户异常';
        }
    }


    /**
     * 财务转账列表
    */
    public function get_finance_list($p){
        if(isset($p['acc_id']) && !empty($p['acc_id'])){
            $wh[] = ['at.out_acc','=',$p['acc_id']];
        }
        if(isset($p['media_id']) && !empty($p['media_id'])){
            $wh[] = ['at.media_id','=',$p['media_id']];
        }
        $wh[] = ['at.status','>',0];
        $arr['list'] = $this->alias('at')
        ->field('at.*,acc.name_id in_name_id,acc2.name_id out_name_id,u.username media_name,ch.name channel_name')
        ->join('op_account acc','at.in_acc = acc.id','Left')
        ->join('op_account acc2','at.out_acc = acc2.id','Left')
        ->join('user u','at.media_id = u.id','Left')
        ->join('channel ch','at.channel_id = ch.id','Left')
        ->where($wh)->order('status,create_time desc')->page($p['page'],$p['limit'])->select();
       
        $arr['cnt'] = $this->alias('at')->where($wh)->count('id');
        return $arr;
    }
    //转账驳回
    public function get_transfer_reject($data){
        $data['status'] = 0;
        $data['audit_id'] = session('uid');
        try {
            $this->update($data);
            return 1;
        } catch (\Throwable $th) {
            //throw $th;
            return $th->getMessage();
        }
    }

    //出纳确认转账转账
    public function get_qr_transfer($data){
        $validate = \think\facade\Validate::rule([
            'audit_url|财务凭证'  => 'require',
            'id|请联系管理员'  => 'require',
        ]);
    
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['status'] = 2;
            try {
                $this->strict(false)->save($data);
                return 1;
            } catch (\Throwable $th) {
                return $th->getMessage();
            }
        }
    }











    
}