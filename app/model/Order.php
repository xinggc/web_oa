<?php
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;
class Order extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time'; 
    //user 媒介
    public function media(){
        return $this->belongsTo(User::class, 'media_id')->bind(['media_name'=>'username']);
    }
    //customer 客户
    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id')->bind(['customer_name'=>'name']);
    }
    public function get_list($p){
        if(isset($p['order_num']) && !empty($p['order_num'])){
            $wh[] = ['order_num','like','%'.$p['order_num'].'%'];
        }
        if(isset($p['sell_time']) && !empty($p['sell_time'])){
            $t = strtotime($p['sell_time']);
            $wh[] = ['sell_time','=',$t];
        }
        if(isset($p['customer_id']) && !empty($p['customer_id'])){
            $wh[] = ['customer_id','=',$p['customer_id']];
        }
        /* if(session('info.is_leader') != 1 && session('info.role_id') !=1 ){
            $wh[] = ['media_id','=',session('uid')];
        } */
        $wh[] = ['id','>',0];
        $arr['list'] = $this->where($wh)->with(['media','customer'])->order('is_jiesuan,create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        return $arr;
    }
    
    
    //添加订单
    public function get_add_order($data){
        $validate = \think\facade\Validate::rule([
            'order_num|订单号'  => 'require|max:50|unique:order',
            'customer_id|客户名称'  => 'require',
            'media_id|媒介人员'  => 'require',
            'sell_time|销售时间'  => 'require',
            'sell|销售单价'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'num|销售数量'  => 'require|number|gt:0',
            'total|销售总价'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'rebates|销售点'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'type|销售类型'  => 'require|number|gt:0',
            'pinzheng|凭证'  => 'require'
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['total'] = $data['num'] * $data['sell'];
            $data['sell_time'] = strtotime($data["sell_time"]);
            $r = $this->save($data);
            return $r;
        }
    }
    //修改订单
    public function get_save_order($data){
        $validate = \think\facade\Validate::rule([
            'customer_id|客户名称'  => 'require',
            'media_id|媒介人员'  => 'require',
            'sell_time|销售时间'  => 'require',
            'sell|销售单价'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'num|销售数量'  => 'require|number|gt:0',
            'total|销售总价'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'rebates|销售点'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'type|销售类型'  => 'require|number|gt:0',
            'pinzheng|凭证'  => 'require'
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['total'] = $data['num'] * $data['sell'];
            $data['sell_time'] = strtotime($data["sell_time"]);
            $r = $this->update($data);
            return $r;
        }
    }
    
    
    /* 
     * 财务结算
     *  */
    public function get_jiesuan($ids){
        $wh[] = ['id','in',$ids];
        $rs = $this->where($wh)->find();
        $wh[] = ['is_jiesuan','=',0];
        $total = $this->where($wh)->sum('total');
        
        $money = Customer::where('id',$rs['customer_id'])->value('prepare_money');
        if($money >= $total){
            return $total;
        }else{
            return -1;
        }
    }
    
    //财务销售统计
    public function get_sell_stat($p){
        if(isset($p['sell_time']) && empty($p['sell_time'])){
            
        }else{
            $t1 = strtotime(date("Y-m-1",time()));
            $s = [$t1,time()];
            $wh[] = ['sell_time','between',$s];
        }
        
        $arr['list'] = $this->field('customer_id,media_id,sell_time,sum(num) nums,sum(total) totals')->where($wh)->with(['media','customer'])->group('customer_id')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->where($wh)->group('customer_id')->count('id');
        return $arr;
    }
    
    
    
    
    
}