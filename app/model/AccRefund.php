<?php
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;
class AccRefund extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
     //账户表
    public function acc(){
        return $this->belongsTo(OpAccount::class, 'op_account_id')->bind(['account_name'=>'name','name_id']);
    }
    
    //修改账户退款
    public function get_save_acc_refund($data){
        $validate = \think\facade\Validate::rule([
            'media_id|操作人员'  => 'require',
            'r_currency|应退账户币'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
          
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['oper_name'] = session('info.username');
            return $this->where('id',$data['id'])->strict(false)->save($data);
        }
    }
    
}