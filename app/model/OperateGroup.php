<?php
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;
class OperateGroup extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    //小组列表
    public function get_list($p){
        $arr['list'] = $this->alias('og')
        ->field('og.*,u.username')
        ->join('user u','og.leader = u.id','LEFT')
        ->page($p['page'],$p['limit'])->select();
        foreach ($arr['list'] as $k=>$v){
            $arr['list'][$k]['member'] = $this->get_users($v['member']);
        }
        $arr['cnt'] = $this->alias('og')->count('og.id','LEFT');
        return $arr;
    }

    function get_users($ids){
        $wh[] = ['id','in',$ids];
        $names = User::where($wh)->column('username');
        $names = implode('，',$names);
        return $names;
    }
    //添加小组
    function get_add($data){
        $validate = \think\facade\Validate::rule([
            'name|小组名称'  => 'require',
            'leader|组长'  => 'require',
            'member|小组成员'  => 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            try {
                $this->save($data);
                return 1;
            } catch (\Throwable $th) {
                //throw $th;
                return $th->getMessage();
            }
            
        }
    }
    //修改小组
    function get_save($data){
        $validate = \think\facade\Validate::rule([
            'name|小组名称'  => 'require',
            'leader|组长'  => 'require',
            'member|小组成员'  => 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            try {
                $this->update($data);
                return 1;
            } catch (\Throwable $th) {
                //throw $th;
                return $th->getMessage();
            }
            
        }
    }










}