<?php

namespace app\model;

use think\Db;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\View;
/**
 * @mixin think\Model
 */
class User extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $type = [
        'last_login_time' => 'timestamp',
    ];

    /**
     * 获取性别
     */
    public function getGenderAttr($val)
    {
        $arr = [0 => '女', 1 => '男'];
        if (!in_array($val, array_keys($arr))) {
            return '未知';
        }
        return $arr[$val];
    }

    //获取相关角色列表
    public function get_role_list($type = 0)
    {
        if ($type > 0) {
            $wh[] = ['role_id', 'in', $type];
        }
        //$wh[] = ['status', '=', 1];
        return $this->field('id,username')->where($wh)->select();
    }

    /**
     * 第三方登录关联
     */
    public function qywx()
    {
        return $this->hasOne('user_qywx', 'id', 'qywx_id');
    }

    /**
     * 获取用户角色ids
     *
     * @return array
     */
    public function role_ids()
    {
        return UserRole::where(['user_id' => $this->id])->column('role_id');
    }


    public function rlerselect()
    {
        $base = Db::name('role')->select();
        return $this->$base;
    }

    /**
     * 用户角色信息
     */
    public function role()
    {
        return $this->hasOne('role', 'id', 'role_id')->bind(['role_name' => 'name']);
    }

    /**
     * 用户职级信息
     */
    public function grade()
    {
        return $this->hasOne('Grade', 'id', 'grade_id')->bind(['grade_name' => 'name']);
    }

    /*
     * 用户部门信息
     */
    public function dept()
    {
        return $this->hasOne('Dept', 'id', 'dept_id')->bind(['dept_name' => 'name']);
    }
    // 获取各部门人员
    public function dept_user_info($dept){
        $wh[] = ['status','=',1];
        $wh[] = ['dept_id','=',$dept];
        $userlist = User::field('id,username')->where($wh)->select();
        View::assign('userlist',$userlist);
    }
}
