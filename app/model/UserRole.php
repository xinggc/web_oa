<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class UserRole extends Model
{

    public function role()
    {
        return $this->hasOne('role', 'id');
    }

}
