<?php
namespace app\model;
use think\Model;
class OpAccount extends Model{
    
    //行业
    public function industry(){
        return $this->belongsTo('industry', 'industry_id')->bind(['industry_name'=>'name']);
    }
    /*  
     * 获取账户列表
     * type 0:全部 1：过滤已被占用的账户
     * $channel 渠道条件
     * */
    public function get_acc_list($type=0,$channel=0){
        
        if($type == 1){
            $acc_list = $this->get_occupy_acc();
            $wh[] = ['id','not in',$acc_list];
        }
        if($channel>0){
            $wh[] = ['channel_id','=',$channel];
        }
        if(session('info.role_id') !=20){
            if(session('info.is_leader') != 1 && session('info.role_id') !=1){
                $wh[] = ['operate_id','=',session('uid')];
            }
        }
        
        $wh[] = ['status','=',1];
        $wh[] = ['jindu','=',5];
        $list = $this->where($wh)->select();
        return $list;
    }
    //获取已被占用的账户
    function get_occupy_acc(){
        $wh[] = ['is_deliver','=',1];
        $wh[] = ['is_finish','=',0];
        $Items = new Items();
        $acc_list = $Items->where($wh)->column('op_account_id');
        $acc_list = implode(',',$acc_list);
        return $acc_list;
    }
    
    //递归获取列表
    function get_dg_list($pid=0){
        $acc_list = $this->get_occupy_acc();
        $wh[] = ['id','not in',$acc_list];
        $wh[] = ['pid','=',$pid];
        $list = $this->field('id,name,cert')->where($wh)->select();
        foreach ($list as $k=>$v){
            $list[$k]['child'] = $this->get_dg_list($v["id"]);
        }
        return $list;
    }

    


    
   
}