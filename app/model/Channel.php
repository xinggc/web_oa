<?php
namespace app\model;
use think\Model;
class Channel extends Model{
    //关联模型
    //平台
    public function platform()
    {
        return $this->belongsTo('platform','pid')->bind(['platform_name'=>'name']);
    }
    //列表
    public function get_list($p){
        
        $list = $this->order("status desc")->page($p['page'],$p['limit'])->select();
        return $list;
    }
    //添加
    public function get_add($data){
        $validate = \think\facade\Validate::rule([
            'pid|平台名称'  => 'require',
            'name|渠道名称'  => 'require|max:50|unique:channel',
            'rebate|渠道返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/'
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['media_id'] = session('uid');
            return $this->save($data);
        }
        
    }
    //修改
    public function get_save($data){
        $validate = \think\facade\Validate::rule([
            'pid|平台名称'  => 'require',
            'name|渠道名称'  => 'require|max:50|unique:channel',
            'rebate|渠道返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/'
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return $this->where('id',$data['id'])->save($data);
            
        }
        
    }
    
    
    //获取 渠道+（平台）
    public function get_channel_platform(){
        $list = $this->with('platform')->select();
        return $list;
    }
    
    
    //通过账户id，获取渠道信息
    public function get_acc_channel_info($acc_id){
        $id = OpAccount::where('id',$acc_id)->value('channel_id');
        return $this->where('id',$id)->find();
    }
    
    
    
    
    
    
    
    
    
    
    
    
}