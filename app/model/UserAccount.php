<?php
namespace app\Model;
use think\Model;
class UserAccount extends Model{
    
    //所有账号列表
    public function list_user_acc_all(){
        $wh[] = ['uid','=',session('uid')];
        $wh[] = ['status','=',1];
        return $this->where($wh)->select();
    }
    
    //添加
    public function add_zfb($data){
        if($data['type'] == 1){
            $validate = \think\facade\Validate::rule([
                'name|户名'  => 'require|max:50',
                'number|账户'  => 'require|max:50',
                'type|类型'  => 'require'
            ]);
        }else{
            $validate = \think\facade\Validate::rule([
                'name|户名'  => 'require|max:50',
                'number|账户'  => 'require|max:50|number',
                'type|类型'  => 'require',
                'bank_name|银行名称'  => 'require'
            ]);
        }
        
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return $this->save($data);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}