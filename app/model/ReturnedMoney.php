<?php
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;
class ReturnedMoney extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    //关联模型
    //关联充值表
    public function recharge()
    {
        return $this->belongsTo(Recharge::class,'pid')->bind(['amount','account_bank_id']);
    }
    //关联媒介
    public function user()
    {
        return $this->belongsTo(User::class,'media_id')->bind(['media_name'=>'username']);
    }
    //关联客户
    public function customer(){
        return $this->belongsTo(Customer::class,'customer_id')->bind(['customer_name'=>'name']);
    }
    //关联公司账户
    public function account(){
        return $this->belongsTo(Account::class,'account_bank_id')->bind(['num','type','bank_address','account_name'=>'name']);
    }
    
    //垫款列表
    public function get_advance_list($p){ 
        if(session('info.is_leader') != 1 && session('info.role_id') !=1 ){
            $wh[] = ['media_id','=',session('uid')];
        }
        if(isset($p['customer_id']) && !empty($p['customer_id'])){
            $wh[] = ['customer_id','=',$p['customer_id']];
        }else{
            $wh[] = ['id','>',0];
        }
        
        $arr['list'] = $this->where($wh)->with(['recharge','user','customer','account'])->order('status,update_time desc,id desc')->page($p['page']*1,$p['limit']*1)->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        return $arr;
    }
    
    
    
    
    /* 财务  回款管理 */
    public function finance_return_money_list($p){
        if(isset($p['customer_id']) && !empty($p['customer_id'])){
            $wh[] = ['rm.customer_id','=',$p['customer_id']];
        }
        if(isset($p['media_id']) && !empty($p['media_id'])){
            $wh[] = ['rm.media_id','=',$p['media_id']];
        }
        $wh[] = ['rm.id','>',0];
        $arr['list'] = $this->with(['recharge','account'])
        ->alias('rm')->field('rm.*,u.username media_name,cu.name customer_name')
        ->join('user u','rm.media_id = u.id',"LEFT")
        ->join('customer cu','rm.customer_id = cu.id',"LEFT")
        ->where($wh)->order('rm.status')->page($p['page']*1,$p['limit']*1)->select();
        $arr['cnt'] = $this->alias('rm')
        ->join('user u','rm.media_id = u.id',"LEFT")
        ->join('customer cu','rm.customer_id = cu.id',"LEFT")->where($wh)->count('rm.id');
        return $arr;
    }
    
    
    
    
    
    
    
}