<?php
namespace app\Model;
use think\Model;
class Claim extends Model{
    //user_account,员工银行账号表
    public function account(){
        return $this->belongsTo('user_account', 'user_account_id')->bind(['bank_name'=>'name','number']);
    }
    //user 员工表
    public function users(){
        return $this->belongsTo('user', 'user_id')->bind(['username']);
    }
    //添加报销
    public function add_user_bx($data){
        $validate = \think\facade\Validate::rule([
            'money|报销金额'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'title|报销类别'  => 'require',
            'remark|费用明细'  => 'require',
            'user_account_id|收款账号'=> 'require',
            'screenshot_url|凭证'=> 'require'
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return $this->save($data);
        }
    }
    //修改报销
    public function save_user_bx($data){
        $validate = \think\facade\Validate::rule([
            'money|报销金额'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'title|报销类别'  => 'require',
            'remark|费用明细'  => 'require',
            'user_account_id|收款账号'=> 'require',
            'screenshot_url|凭证'=> 'require'
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return $this->where('id',$data['id'])->save($data);
        }
    }
    //审批 待处理列表
    public function get_list_sp_pending($p){
        $wh[] = ['status','in','1,2'];
        $wh[] = ['user_id','=',session('uid')];
        $arr['list'] =  $this->where($wh)->order('create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        return $arr;
    }
    //审批 完成列表
    public function get_list_sp_accomplish($p){
        $wh[] = ['status','=',3];
        $wh[] = ['user_id','=',session('uid')];
        $arr['list'] =  $this->with(['account'])->where($wh)->order('create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        return $arr;
    }
    
    //添加预支明细
    public function get_add_yuzhi($data){
        $validate = \think\facade\Validate::rule([
            'money|预支金额'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'title|预支类别'  => 'require',
            'remark|预支明细'  => 'require',
            'employ_time|使用时间'=> 'require',
            'user_account_id|收款账号'=> 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return $this->save($data);
        }
    }
    
    //修改预支明细
    public function get_save_yuzhi($data){
        $validate = \think\facade\Validate::rule([
            'money|预支金额'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'title|预支类别'  => 'require',
            'remark|预支明细'  => 'require',
            'employ_time|使用时间'=> 'require',
            'user_account_id|收款账号'=> 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return $this->where('id',$data['id'])->save($data);
        }
    }
   
    //财务 获取报销列表
    public function list_caiwu_daibaoxiao($data){
        
        if(!empty($data['uid'])){
            $wh[] = ['user_id','=',$data['uid']];
        }
        $wh[] = ['status','in','1,2,3'];
        $arr['list'] = $this->with(['account'])->where($wh)->order('status,create_time desc')->page($data['page'],$data['limit'])->select();
        $arr['cnt'] = $this->count('id');
        return $arr;
    }
    
    
    
    //获取相关申请人信息
    public function get_cashier_user_info($id){
        $wh[] = ['id','=',$id];
        return $this->with(['account'])->where($wh)->find();
    }
    
    //更新出纳提交
    public function save_cashier($data){
        $validate = \think\facade\Validate::rule([
            'accomplish_url|凭证上传'  => 'require',
            'id|此条数据'=> 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return $this->where('id',$data['id'])->save($data);
        }
        
    }
    
    
    
    
    
    
    
    
}