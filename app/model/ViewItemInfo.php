<?php
namespace app\model;
use think\Model;
class ViewItemInfo extends Model{
    
    //消耗统计列表
    public function consume_list($p){
        
        if(isset($p['op_account_id'])&&!empty($p['op_account_id'])){
            $wh[] = ['op_account_id','=',$p['op_account_id']];
        }else{
            $acc_ids = Items::group('op_account_id')->column('op_account_id');
            $wh[] = ['op_account_id','in',$acc_ids];
        }
        if(isset($p['operate_id'])&&!empty($p['operate_id'])){
            $wh[] = ['operate_id','=',$p['operate_id']];
        }
        if(isset($p['media_id'])&&!empty($p['media_id'])){
            $wh[] = ['media_id','=',$p['media_id']];
        }
        
        $t = date("Y-m-01",time());
        $t1 = strtotime($t);
        $t2 = strtotime("$t +1 month -1 day");
        if(isset($p['time'])&&!empty($p['time'])){
            /* $t1 = strtotime($p['time']);
            $t2 = strtotime($p['time']." +1 month -1 day"); */
            $array=explode('&', $p['time']);
            $t1 = strtotime($array[0]);
            $t2 = strtotime($array[1]);
        }
        
        $wh[] = ['start_time','<=',$t2];
        $wh[] = ['end_time','>=',$t1];
       
        $arr['list'] = $this->where($wh)->order('start_time')->page($p['page']*1,$p['limit']*1)->select();
        foreach ($arr['list'] as $k=>$v){
            $b = $this->get_total_consume($v['items_id'],$t1,$t2,$v['op_account_id']);
            $arr['list'][$k]['total_consume'] = $b['count'];
            $arr['list'][$k]['consume_profit'] = $b['profit'];
            $a = $this->get_total_currency($v['items_id'],$t1,$t2);
            $arr['list'][$k]['total_currency'] = $a['count'];
            $arr['list'][$k]['profit'] = $a['profit'];
            $c = $this->get_sell_profit($v['items_id'],$t1,$t2);
            $arr['list'][$k]['sell_rebates'] = $c['profit'];
            $arr['list'][$k]['amount'] = $this->get_total_recharge($v['customer_id'],$t1,$t2);
            $arr['list'][$k]['cu_refund'] = $this->get_total_recharge($v['customer_id'],$t1,$t2,3);
            $arr['list'][$k]['acc_refund'] = $this->get_total_opacc_refund($v['op_account_id'],$v['items_id'],$t1,$t2);
        }
      
        $arr['cnt'] = $this->where($wh)->count('id');
        
        return $arr;
    }
    
    
    //获取总消耗（账户币）
    public function get_total_consume($item_id,$t1,$t2,$op_account_id){
        $wh[] = ['time','>=',$t1];
        $wh[] = ['time','<=',$t2];
        $wh[] = ['items_id','=',$item_id];
        $wh[] = ['op_account_id','=',$op_account_id];
        $arr['count'] = ItemConsume::where($wh)->sum('currency');
        $arr['profit'] = ItemConsume::where($wh)->sum('consume_profit');
        return $arr;
    }
    //获取总充值（账户币）
    public function get_total_currency($item_id,$t1,$t2){
        $wh[] = ['create_time','>=',$t1];
        $wh[] = ['create_time','<=',$t2];
        $wh[] = ['items_id','=',$item_id];
        $wh[] = ['status','=',1];
        $wh[] = ['type','=',2];
        $arr['count'] = Recharge::where($wh)->sum('practical_currency');
        $arr['profit'] = Recharge::where($wh)->sum('profit');
        return $arr;
    }
    //获取账户退款（账户币）
    function get_total_opacc_refund($op_account_id,$items_id,$t1,$t2){
        $w[] = ['op_account_id','=',$op_account_id];
        $w[] = ['items_id','=',$items_id];
        $ids = Recharge::where($w)->column('id');
        $wh[] = ['create_time','>=',$t1];
        $wh[] = ['create_time','<=',$t2];
        $wh[] = ['pid','in',$ids];
        $wh[] = ['status','=',2];
        $r_currency = AccRefund::where($wh)->sum('r_currency');
        return $r_currency;
    }
    //获取总打款:1, 退款：3
    public function get_total_recharge($customer_id,$t1,$t2,$type=1){
        $wh[] = ['create_time','>=',$t1];
        $wh[] = ['create_time','<=',$t2];
        $wh[] = ['customer_id','=',$customer_id];
        $wh[] = ['status','=',1];
        $wh[] = ['type','=',$type];
        $amount = Recharge::where($wh)->sum('amount');
        return $amount;
    }
    //获取销售利润
    public function get_sell_profit($item_id,$t1,$t2){
        $wh[] = ['create_time','>=',$t1];
        $wh[] = ['create_time','<=',$t2];
        $wh[] = ['items_id','=',$item_id];
        $wh[] = ['status','=',1];
        $wh[] = ['type','=',2];
        $list  = Recharge::field('currency,rebate,service_pay,sell_rebates,is_fans')->where($wh)->select()->toArray();
        $profit = 0;
        foreach ($list as $k=>$v){
            $profit += $this->sum_sell_profit($v['currency'],$v['rebate'],$v['service_pay'],$v['sell_rebates'],$v['is_fans']);
        }
        $arr['profit'] = $profit;
        
        return $arr;
    }
    //计算销售利润
    function sum_sell_profit($currency,$rebate,$service,$sell_rebates,$is_fans){
       
        if($rebate > 0){  //客户返点
            $f = 1 + $rebate/100;
            $l1 = $currency/$f;
        }
        if($service > 0){ //服务费
            $f = 1 + $service/100;
            $l1 = $currency*$f;
        }
        if($rebate ==0 && $service == 0){
            return 0;
        }
        $l2 = $currency/(1+$sell_rebates/100);
       
        
        $l = $l1 - $l2;
        return round($l,2);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}