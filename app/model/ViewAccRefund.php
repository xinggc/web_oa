<?php
namespace app\model;
use think\Model;
use think\trace\TraceDebug;
use think\model\concern\SoftDelete;
class ViewAccRefund extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    public function get_refund_list($p){ 
        if(session('info.is_leader') != 1 && session('info.role_id') !=1 ){
            $wh[] = ['media_id','=',session('uid')];
        }
        if(isset($p['customer_id']) && !empty($p['customer_id'])){
            $wh[] = ['customer_id','=',$p['customer_id']];
        }
        if(isset($p['name']) && !empty($p['name'])){
            $wh[] = ['op_account_id','=',$p['name']];
        }else {
            $wh[] = ['id','>',0];
        }
        //$wh[] = ['delete_time','null'];
        $arr['list'] = $this->where($wh)->order('status ,update_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        return $arr;
    }
    
    /* 
     * 财务
     *  */
    //账户退款审核利列表
    public function get_cw_refund_list($p){
        if(isset($p['acc_id'])&& !empty($p['acc_id'])){
            $wh[] = ['op_account_id','=',$p['acc_id']];
        }
        if(isset($p['channel_id'])&& !empty($p['channel_id'])){
            $wh[] = ['channel_id','=',$p['channel_id']];
        }
        if(isset($p['customer_id'])&& !empty($p['customer_id'])){
            $wh[] = ['customer_id','=',$p['customer_id']];
        }
        if(isset($p['media_id'])&& !empty($p['media_id'])){
            $wh[] = ['media_id','=',$p['media_id']];
        }
        if(isset($p['time'])&& !empty($p['time'])){
            $t = explode('&',$p['time']); 
            $wh[] = ['create_time','between',[strtotime($t[0]),strtotime($t[1])+86399]];
        }else{
            $wh[] = ['id','>',0];
        }
        
        $wh[] = ['status','in','1,2'];
        $arr['list'] = $this->where($wh)->order('id desc,update_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        return $arr;
    }
}