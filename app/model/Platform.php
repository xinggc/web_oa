<?php
namespace app\Model;
use think\Model;

class Platform extends Model{
    
    //列表
    public function get_list($p){
       
        $list = $this->order("status desc")->page($p['page'],$p['limit'])->select();
        return $list;
    }
    //添加平台
    public function get_add($data){
        $validate = \think\facade\Validate::rule([
            'name|平台名称'  => 'require|max:50|unique:platform'
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['media_id'] = session('uid');
            return $this->save($data);
        }
        
    }
    //添加平台
    public function get_save($data){
        $validate = \think\facade\Validate::rule([
            'name|平台名称'  => 'require|max:50|unique:platform'
        ]);
    
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return $this->where('id',$data['id'])->save($data);
            
        }
    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}