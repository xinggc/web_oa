<?php
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;
use app\model\AccTransfer;
class Recharge extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    //渠道表
    public function qd(){
        return $this->belongsTo(Channel::class, 'channel_id')->bind(['channel_name'=>'name']);
    }
    //客户表
    public function client(){
        return $this->belongsTo(Customer::class, 'customer_id')->bind(['client_name'=>'name']);
    }
    //user表媒介
    public function media(){
        return $this->belongsTo(User::class, 'media_id')->bind(['media_name'=>'username']);
    }
    //账户表
    public function acc(){
        return $this->belongsTo(OpAccount::class, 'op_account_id')->bind(['account_name'=>'name','name_id']);
    }
    //user表财务
    public function audit(){
        return $this->belongsTo(User::class, 'audit_id')->bind(['audit_name'=>'username']);
    }
    //user 运营
    public function operate(){
        return $this->belongsTo(User::class, 'operate_id')->bind(['operate_name'=>'username']);
    }
    
    

    
    
    //客户充值添加
    public function get_cz_add($data){
        $data['md5_screenshot_url'] = md5_file($data['screenshot_url']);
        $validate = \think\facade\Validate::rule([
            'code|充值编号'  => 'require',
            'customer_id|客户姓名'  => 'require',
            'account_bank_id|充值卡号'  => 'require',
            // 'amount|打款金额'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'amount|打款金额'  => 'require|regex:/^-?[0-9]+(.[0-9]{1,3})?$/',
            'media_id|媒介人员'  => 'require',
            'screenshot_url|凭证'  => 'require',
            'md5_screenshot_url|凭证图片'  => 'require|unique:recharge',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['oper_name'] = session('info.username');
            return $this->save($data);
        }
        
    }
    
    //客户打款修改
    public function get_cz_save($data){
        $data['md5_screenshot_url'] = md5_file($data['screenshot_url']);
        $validate = \think\facade\Validate::rule([
            'media_id|媒介人员'  => 'require',
            'code|充值编号'  => 'require',
            'customer_id|客户姓名'  => 'require',
            'account_bank_id|公司卡号'  => 'require',
            'amount|充值金额'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'screenshot_url|凭证'  => 'require',
            'md5_screenshot_url|凭证图片'  => 'require|unique:recharge',
        ]);
    
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['oper_name'] = session('info.username');
            return $this->where('id',$data["id"])->strict(false)->save($data);
        }
    
    }
    
    //账户充值添加
    public function get_acc_add($data){
        $v =[
            'media_id|操作人员'  => 'require',
            'code|充值编号'  => 'require',
            'channel_id|渠道名称'  => 'require',
            'op_account_id|账户名称'  => 'require',
            'items_id|项目名称'  => 'require',
            'customer_id|客户姓名'  => 'require',
            'sell_rebates|销售返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            
        ]; 
        $v['rebate|返点'] = 'regex:/^-?[0-9]+(.[0-9]{1,3})?$/';
        //$v['service_pay|服务费'] = 'regex:/^[0-9]+(.[0-9]{1,3})?$/';
        $v['channel_rebate|渠道返点'] = 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/';
        $v['currency|账户币'] = 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/';
        $v['amount|充值金额'] = 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/';
      
        $validate = \think\facade\Validate::rule($v);
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
           
            //账户充值判断客户余额是否充足
            // if(!$this->is_acc_re_money($data['customer_id'],$data['amount'])){
            //     return '客户余额不足';
            // }else{
            //     return true;
            // }
            return true;
            
        }
    }
    //修改账户充值
    public function get_acc_save($data){
       
        $v['rebate|返点'] = 'regex:/^-?[0-9]+(.[0-9]{1,3})?$/';
        $v['sell_rebates|销售返点'] = 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/';
        $v['channel_rebate|渠道返点'] = 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/';
        $v['currency|账户币'] = 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/';
        $v['amount|充值金额'] = 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/';
        $v['media_id|媒介'] = 'require';

        $validate = \think\facade\Validate::rule($v);
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return true;
            
        }
    }
    //账户充值判断客户余额是否充足
    function is_acc_re_money($cu_id,$amount){
        $prepare_money = $this->hasWhere('client', ['id'=>$cu_id])->value("prepare_money");
       
        if($prepare_money*1 >= $amount*1){
            return true;
        }else{
            return false;
        }
    }
    
    
    
    //账户充值列表
    public function get_acc_pay_list($w=0,$type=2,$p){ 
        if($w){
            $wh[] = ['op_account_id','=',$w];
        }
        if(isset($p['customer_id']) && !empty($p['customer_id'])){
            $wh[] = ['customer_id','=',$p['customer_id']];
        }
        if(isset($p['media_id']) && !empty($p['media_id'])){
            $wh[] = ['media_id','=',$p['media_id']];
        }else{
            if(session('info.is_leader') != 1 && session('info.role_id') !=1 ){
                $wh[] = ['media_id','=',session('uid')];
            }
        }
        $wh[] = ['type','=',$type];
        $wh[] = ['is_paid','=',0];
        
        $list = $this
        ->with([
            'qd', 'media', 'client','acc'
        ])->where($wh)->order('update_time desc,id desc')->page($p['page']*1,$p['limit']*1)->select();
        
        return $list;
    }
    //账户退款列表
    public function get_acc_succeed_list($p){
        if(isset($p['customer_id']) && !empty($p['customer_id'])){
            $wh[] = ['r.customer_id','=',$p['customer_id']];
        }
        if(isset($p['name']) && !empty($p['name'])){
            $wh[] = ['r.op_account_id','=',$p['name']];
        }
        $wh[] = ['r.type','=','4'];
        $arr['list'] = $this->alias('r')
        ->field('r.*,ch.name channel_name,acc.name_id,cu.name customer_name,u.username media_name')
        ->join('channel ch','r.channel_id = ch.id','LEFT')
        ->join('op_account acc','r.op_account_id = acc.id','LEFT')
        ->join('customer cu','r.customer_id = cu.id','LEFT')
        ->join('user u','r.media_id = u.id','LEFT')
        ->where($wh)->order('r.status, r.create_time desc')->page($p['page']*1,$p['limit']*1)->select();
        $arr['cnt'] = $this->alias('r')->where($wh)->count('r.id');
        return $arr;
    }
    // 账户退款添加
    public function add_acc_refund($data){
        $data['md5_screenshot_url'] = md5_file($data['screenshot_url']);
        $validate = \think\facade\Validate::rule([
            'op_account_id|退款账户'  => 'require',
            'rebate|客户返点'  => 'require|regex:/^-?[0-9]+(.[0-9]{1,3})?$/',
            'channel_rebate|端口返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'sell_rebates|销售返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'currency|应退账户币'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'media_id|媒介'  => 'require',
            'screenshot_url|退款凭证'  => 'require',
            'md5_screenshot_url|凭证图片'  => 'require|unique:recharge',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['type'] = 4;
            $data['status'] = 0;
            $data['amount'] = zhb_cny($data['currency'],$data['rebate']);
            $data['channel_money'] = zhb_cny($data['currency'],$data['channel_rebate']);
            $data['profit'] = cny_profit($data['currency'],$data['rebate'],$data['channel_rebate']);
            if($data['sell_rebates'] > 0){
                $data['sell_profit'] = cny_profit($data['currency'],$data['rebate'],$data['sell_rebates']);
            }else{
                $data['sell_profit'] = 0;
            }
            // $data['items_id'] = $r['items_id'];
            $data['create_time'] = strtotime($data['create_time']);
            try {
                $this->strict(false)->save($data);
                return 1;
            } catch (\Throwable $th) {
                //throw $th;
                return $th->getMessage();
            }
            
        }
    }
    //账户退款修改
    public function get_save_acc_refund($data){
        $data['md5_screenshot_url'] = md5_file($data['screenshot_url']);
        $validate = \think\facade\Validate::rule([
            'rebate|客户返点'  => 'require|regex:/^-?[0-9]+(.[0-9]{1,3})?$/',
            'channel_rebate|端口返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'sell_rebates|销售返点'  => 'require|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'currency|应退账户币'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'media_id|媒介'  => 'require',
            'screenshot_url|退款凭证'  => 'require',
            'md5_screenshot_url|凭证图片'  => 'require|unique:recharge',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            //$r = Recharge::where('pid',$data['pid'])->find();
            
            $data['amount'] = zhb_cny($data['currency'],$data['rebate']);
            $data['channel_money'] = zhb_cny($data['currency'],$data['channel_rebate']);
            $data['profit'] = cny_profit($data['currency'],$data['rebate'],$data['channel_rebate']);
            if($data['sell_rebates'] > 0){
                $data['sell_profit'] = cny_profit($data['currency'],$data['rebate'],$data['sell_rebates']);
            }else{
                $data['sell_profit'] = 0;
            }
            
            $data['oper_name'] = session('info.username');
            $data['status'] = 0;
            $data['create_time'] = strtotime($data['create_time']);
            try {
                $this->where('id',$data['id'])->strict(false)->save($data);
                return 1;
            } catch (\Throwable $th) {
                //throw $th;
                return $th->getMessage();
            }
            
        }
    }
    
    //添加客户退款
    public function get_add_cu_refund($data){
        $validate = \think\facade\Validate::rule([
            'media_id|操作人员'  => 'require',
            'code|退款编号'  => 'require',
            'customer_id|客户姓名'  => 'require',
            'amount|退款金额'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            
            return true;
        }
    }
    //修改客户退款
    public function get_save_cu_refund($data){
        $validate = \think\facade\Validate::rule([
            'media_id|操作人员'  => 'require',
            'code|退款编号'  => 'require',
            'customer_id|客户姓名'  => 'require',
            'amount|退款金额'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            
            return true;
        }
    }
    
    //客户退款列表
    public function get_refunc_list($w,$p){
        if($w){
            $wh[] = ['customer_id','=',$w];
        }
        if(session('info.is_leader') != 1 && session('info.role_id') !=1 ){
            $wh[] = ['media_id','=',session('uid')];
        }
        $wh[] = ['type','=',3];
        $wh[] = ['is_paid','=',0];
        $arr['list'] = $this ->with(['media', 'client','audit' ])
        ->where($wh)->order('status,create_time desc')->page($p['page']*1,$p['limit']*1)->select();
        $arr['cnt'] = $this ->with(['media', 'client','audit' ])->where($wh)->count('id');
        
        return $arr;
    }
    
    //财务 获取充值申请列表
    public function get_list_apply_re($p,$type){
        if(isset($p['cu_id']) && !empty($p['cu_id'])){
            $wh[] = ['r.customer_id','=',$p['cu_id']];
        }
        if(isset($p['media_id']) && !empty($p['media_id'])){
            $wh[] = ['r.media_id','=',$p['media_id']];
        }
        if(isset($p['acc_id']) && !empty($p['acc_id'])){
            $wh[] = ['r.op_account_id','=',$p['acc_id']];
        }
        if(isset($p['time']) && !empty($p['time'])){ 
            $t = explode('&',$p['time']); 
            $wh[] = ['r.create_time','between',[strtotime($t[0]),strtotime($t[1])+86399]];
        }else{
            $t = strtotime('-30 day');
            $wh[] = ['r.create_time','between',[$t,time()]];
        }
        if(isset($p['channel_id']) && !empty($p['channel_id'])){  
            $wh[] = ['r.channel_id','=',$p['channel_id']];
        }
        if(isset($p['industry_id']) && !empty($p['industry_id'])){  
            $wh[] = ['hy.id','=',$p['industry_id']];
        }
        if(isset($p['items_id']) && !empty($p['items_id'])){  
            $wh[] = ['r.items_id','=',$p['items_id']];
        }
        if($type == 1){
            $wh[] = ['r.type','=',1];
        }elseif($type == 2){
            $wh[] = ['r.type','=',2];
        }
        

        $wh[] = ['r.status','in','0,1'];
        $wh[] = ['r.is_paid','=',0];
        $arr['list'] = $this->with(['qd', 'media', 'client'])
        ->alias('r')->field('r.*,acc.name_id,acc.name acc_name,hy.name hy_name,i.name item_name')
        ->join('op_account acc','r.op_account_id = acc.id','LEFT')
        ->join('industry hy','acc.industry_id = hy.id','LEFT')
        ->join('items i','r.items_id = i.id','LEFT')
        ->where($wh)->order('r.status,r.create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->alias('r')
        ->join('op_account acc','r.op_account_id = acc.id','LEFT')
        ->join('industry hy','acc.industry_id = hy.id','LEFT')->where($wh)->count('r.id');
        return $arr;
    }
    //财务 获取客户退款申请
    public function get_list_cu_refund($p){
        if(isset($p["cu_id"]) && !empty($p['cu_id'])){
            $wh[] = ['customer_id','=',$p["cu_id"]];
        }
        if(isset($p['time']) && !empty($p['time'])){
            $t = explode('&',$p['time']); 
            $wh[] = ['create_time','between',[strtotime($t[0]),strtotime($t[1])+86399]];
        }
        $wh[] = ['type','=',3];
        $wh[] = ['status','in','0,1'];
        $wh[] = ['is_paid','=',0];
        $arr['list'] = $this->with(['qd', 'media', 'client','acc','audit'])
        ->where($wh)->order('status,create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        return $arr;
    }
    
    //添加赔付信息
    public function add_paid($data){
        if($data["type"] == 1 || $data["type"] == 3){
            if(empty($data["customer_id"])){
                return '客户名称不能为空';
            }
            if(empty($data["amount"])){
                return '赔付金额不能为空';
            }
        }
        if($data["type"] == 2){
            if(empty($data["channel_id"])){
                return '渠道名称不能为空';
            }
            if(empty($data["op_account_id"])){
                return '账户名称不能为空';
            }
            if(empty($data["currency"])){
                return '赔付账户币不能为空';
            }
        }
        if(empty($data["rebate"]) && empty($data["service_pay"])){
            return '返点比例和服务费比例二选一';
        }
        
        $validate = \think\facade\Validate::rule([
            'type|赔付类型'  => 'require',
            'code|退款编号'  => 'require',
            'media_id|媒介人员'  => 'require',
            'operate_id|运营人员'  => 'require',
            'rebate|返点比例'  => 'gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'service_pay|服务费比例'  => 'gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'amount|赔付金额'  => 'gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
            'currency|赔付账户币金额'  => 'gt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'note|备注'  => 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            $data['is_paid'] = 1;
            //返点
            if(!empty($data['amount']) && !empty($data['rebate'])){
                $data['currency'] = $data['amount'] * ((1+($data['rebate']/100)));
                $data['currency'] =  round($data['currency'],2);
            }
            if(!empty($data['currency']) && !empty($data['rebate'])){
                $data['amount'] = $data['currency'] / ((1+($data['rebate']/100)));
                $data['amount'] =  round($data['amount'],2);
            }
            //服务费
            if(!empty($data['amount']) && !empty($data['service_pay'])){
                $data['currency'] = $data['amount'] * ((1-($data['rebate']/100)));
                $data['currency'] =  round($data['currency'],2);
            }
            if(!empty($data['currency']) && !empty($data['service_pay'])){
                $data['amount'] = $data['currency'] / ((1-($data['rebate']/100)));
                $data['amount'] =  round($data['amount'],2);
            }
            
            return $this->save($data);
        }
    }
    
    //财务赔付记录
    public function get_list_paid($p){
        $wh[] = ['is_paid','=',1];
        $wh[] = ['status','<>',-1];
        $arr['list'] = $this->with([ 'qd', 'media', 'client','acc','operate' ])
                        ->where($wh)->order('status,create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        return $arr;
    }
    
    
    
    //快捷充值
    public function get_kj_cz($data){
        if(!isset($data['currency'])  && empty($data['amount'])){
            return '提交失败，请输入有效的值';
        }
        $wh = [
        'amount|打款金额'  => 'egt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
        'media_id|媒介人员'  => 'require',
        'customer_id|客户ID'  => 'require',
        ];
        
        $money = 0;
        if(isset($data['currency']) && count($data['currency']) > 0){
            for ($i=0;$i<count($data['currency']);$i++){
              
                if($data['currency'][$i]<1 || !preg_match("/^[0-9]+(.[0-9]{1,2})?$/",$data['currency'][$i])){
                    return '第'.$data['name_id'][$i].'组，请输入正确的账户币';
                }
                if(!preg_match("/^-?[0-9]+(.[0-9]{1,2})?$/",$data['rebate'][$i])){
                    return '第'.$data['name_id'][$i].'组，请输入正确的客户返点';
                }
                if(!preg_match("/^[0-9]+(.[0-9]{1,2})?$/",$data['amounts'][$i])){
                    return '第'.$data['name_id'][$i].'组，请输入正确的人民币金额';
                }
                //账户币换算成人民币,计算累计充值人民币
                $money = $money + $data['amounts'][$i];
                if($data['sell_rebates'][$i]<0 || !preg_match("/^[0-9]+(.[0-9]{1,2})?$/",$data['sell_rebates'][$i])){
                    return '第'.$data['name_id'][$i].'组，请输入正确的销售返点';
                }
                if($data['channel_rebate'][$i]<0 || !preg_match("/^[0-9]+(.[0-9]{1,2})?$/",$data['channel_rebate'][$i])){
                    return '第'.$data['name_id'][$i].'组，请输入正确的渠道返点';
                }
              
            }
            
            $prepare_money = Customer::where('id',$data['customer_id'])->value('prepare_money');
            $a = $data['amount']?$data['amount']:0;
            // if(($prepare_money + $a) < $money){
            //     return '客户备款或备款不足。';
            // }
        }
        if($data['amount'] > 0 && $data['is_advance'] == 0){
            $wh['screenshot_url|凭证'] = 'require';
        }
       
        $validate = \think\facade\Validate::rule($wh);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            return true;
        }
    }
    
    /**
     *  财务 账户退款列表
    */
    public function get_finance_tk($p){
        if(isset($p['acc_id'])&& !empty($p['acc_id'])){
            $wh[] = ['r.op_account_id','=',$p['acc_id']];
        }
        if(isset($p['channel_id'])&& !empty($p['channel_id'])){
            $wh[] = ['r.channel_id','=',$p['channel_id']];
        }
        if(isset($p['customer_id'])&& !empty($p['customer_id'])){
            $wh[] = ['r.customer_id','=',$p['customer_id']];
        }
        if(isset($p['media_id'])&& !empty($p['media_id'])){
            $wh[] = ['r.media_id','=',$p['media_id']];
        }
        if(isset($p['time'])&& !empty($p['time'])){
            $t = explode('&',$p['time']); 
            $wh[] = ['r.create_time','between',[strtotime($t[0]),strtotime($t[1])+86399]];
        }
        $wh[] = ['r.type','=',4];
        $wh[] = ['r.status','>',-1];
        $arr['list'] = $this->alias('r')
        ->field('r.*,ch.name channel_name,acc.name_id,cu.name customer_name,u.username media_name')
        ->join('channel ch','r.channel_id = ch.id','LEFT')
        ->join('op_account acc','r.op_account_id = acc.id','LEFT')
        ->join('customer cu','r.customer_id = cu.id','LEFT')
        ->join('user u','r.media_id = u.id','LEFT')
        ->where($wh)->order('r.status,r.create_time desc')->page($p['page'],$p['limit'])->select();
        $arr['cnt'] = $this->alias('r')->where($wh)->count('r.id');
        return $arr;
    }
    
    // 客户打款，充值明细
    public function get_cu_fund_info($id){ 
        $w[] =['at.customer_id','=',$id];
        $zz = AccTransfer::alias('at')
        ->field('at.*,at.in_currency currency,acc.name_id out_account,acc2.name_id in_account')
        ->join('op_account acc','at.out_acc = acc.id','LEFT')
        ->join('op_account acc2','at.in_acc = acc2.id','LEFT')
        ->where($w)->order('at.status,at.create_time desc')->limit(100)->select();
        foreach($zz as $k=>$v){
            $zz[$k]['status']  = $v['status']-1;
        }
        
        $wh[] = ['r.customer_id','=',$id];
        $list = $this->alias('r')
        ->field('r.*,acc.name_id')
        ->join('op_account acc','r.op_account_id = acc.id','LEFT')
        ->where($wh)->order('r.status,r.create_time desc')->limit(100)->select()->merge($zz)->toArray();
        $last_names = array_column($list,'create_time');
        array_multisort($last_names,SORT_DESC,$list);
       
        return $list;
    }
    
    //财务 客户资金明细
    public function get_cu_zijin_mingxi($data){
        if(isset($data['time']) && !empty($data['time'])){
            $time = explode('&', $data['time']);
            $t1 = strtotime($time[0]);
            $t2 = strtotime($time[1]);
        }else{
            $firstday = date('Y-m-01', time());//本月第一天
            $lastday = date('Y-m-d', strtotime("$firstday +1 month -1 day"));
            $t1 = strtotime($firstday);
            $t2 = strtotime($lastday) + 86399;
        }
        $w1[] = ['r.create_time','between',[$t1,$t2]];
        $w1[] = ['r.customer_id','=',$data['id']];
        $w1[] = ['r.status','=',1];
        $list = $this->alias('r')
        ->field('r.*,acc.name_id')
        ->join('op_account acc','r.op_account_id = acc.id','LEFT')
        ->where($w1)->order('create_time desc')->select();
        $arr = [];
        if(!empty($list)){
            foreach ($list as $k=>$v){
                $a['name_id'] = $v['name_id'];
                $a['create_time'] = $v['create_time'];
                $a['note'] = $v['note'];
                if($v['type'] == 1){
                    $a['amount'] = $v['amount'];
                }else{
                    $a['amount'] = '';
                }
                if($v['type'] == 2){
                    $a['currency'] = $v['currency'];
                    $a['cz_currency'] = $v['amount'];
                }else{
                    $a['currency'] = '';
                    $a['cz_currency'] = '';
                }
                if($v['type'] == 3){
                    $a['kh_amount'] = $v['amount'];
                }else{
                    $a['kh_amount'] = '';
                }
                if($v['type'] == 4){
                    $a['zh_amount'] = $v['amount'];
                    $a['zh_currency'] = $v['currency'];
                }else{
                    $a['zh_amount'] = '';
                    $a['zh_currency'] = '';
                }
                $arr[$k] = $a;   
            }
            
        }
        return $arr;
    }
        
    
    
}