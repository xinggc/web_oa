<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class UserQywx extends Model
{

    // 用户模型关联
    public function user()
    {
        return $this->hasOne('user', 'qywx_id', 'id');
    }

}
