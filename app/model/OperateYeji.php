<?php
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;
class OperateYeji extends Model{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    //列表
    public function get_list($p){
        if(isset($p['operate_id']) && !empty($p['operate_id'])){
            $wh[] = ['oy.operate_id','=',$p['operate_id']];
        }
        if(isset($p['time']) && !empty($p['time'])){
            $arr = explode("&",$p['time']);
            $t = strtotime($arr[0]);
            $t2 = strtotime($arr[1]);
        }else{
            $t = strtotime(date("Y-m-01",time()));
            $t2 = strtotime(date("Y-m-01",time())." +1 month -1 day");
        }
       
        $wh[] = ['oy.time','between',[$t,$t2]];
        $wh[] = ['oy.operate_id','>',0];
        $a['list'] = $this->alias('oy')
        ->field('oy.*,u.username')
        ->join('user u','oy.operate_id = u.id','LEFT')
        ->where($wh)->select();
        foreach ($a['list'] as $k=>$v){
            $xh = $this->get_xiaohao_sum($v['operate_id'],$t,$t2);
            $complete = ($xh/$v['yeji'])*100;
            $a['list'][$k]['complete'] = round($complete,2);
            $a['list'][$k]['wancheng'] = $xh;
        }
       
        return $a;
    }
    function get_xiaohao_sum($operate_id,$t,$t2){
       
        $wh[] = ['time','between',[$t,$t2]];
        if($operate_id != 9){
            $wh[] = ['operation_id','=',$operate_id];
        } 
        return ItemConsume::where($wh)->sum('currency');
    }
    //添加
    public function get_add($data){
        $validate = \think\facade\Validate::rule([
            'operate_id|运营名称'  => 'require',
            'time|任务日期'  => 'require',
            'yeji|任务'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            try {
                $data['time'] = strtotime($data['time']);
                $this->save($data);
                return 1;
            } catch (\Throwable $th) {
                //throw $th;
                return $th->getMessage();
            }
            
        }
    }
    //修改业绩
    function get_save($data){
        $validate = \think\facade\Validate::rule([
            'operate_id|运营名称'  => 'require',
            'time|任务日期'  => 'require',
            'yeji|任务'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            try {
                $data['time'] = strtotime($data['time']);
                $this->update($data);
                return 1;
            } catch (\Throwable $th) {
                //throw $th;
                return $th->getMessage();
            }
        }
    }


    // 媒介添加任务
    public function get_add_task($data){
        $validate = \think\facade\Validate::rule([
            'media_id|媒介名称'  => 'require',
            'time|任务日期'  => 'require',
            'yeji|任务'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            try {
                $data['time'] = strtotime($data['time']);
                $this->save($data);
                return 1;
            } catch (\Throwable $th) {
                //throw $th;
                return $th->getMessage();
            }
            
        }
    }
    // 媒介修改任务
    public function get_save_task($data){
        $validate = \think\facade\Validate::rule([
            'media_id|媒介名称'  => 'require',
            'time|任务日期'  => 'require',
            'yeji|任务'  => 'require|gt:0|regex:/^[0-9]+(.[0-9]{1,2})?$/',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
            try {
                $data['time'] = strtotime($data['time']);
                $this->update($data);
                return 1;
            } catch (\Throwable $th) {
                //throw $th;
                return $th->getMessage();
            }
            
        }
    }

    // 数据统计
    //任务列表
    public function get_gr_rw($p){
        if(isset($p['time']) && !empty($p['time'])){
            $t = strtotime($p['time']);
            $t2 = strtotime($p['time']." +1 month -1 day");
        }else{
            $t = strtotime(date("Y-m-01",time()));
            $t2 = strtotime(date("Y-m-01",time())." +1 month -1 day");
        }
        
        $wh[] = ['oy.time','between',[$t,$t2]];
        $wh[] = ['oy.operate_id','>',0];
        $wh[] = ['oy.operate_id','<>',9];
        $a['list'] = $this->alias('oy')
        ->field('oy.yeji,u.username')
        ->join('user u','oy.operate_id = u.id','LEFT')
        ->where($wh)->select();
        foreach ($a['list'] as $k=>$v){
            $name = preg_replace('/牧唐-运营助理-/','',$v['username']);
            if(strlen($name)>9){
                $name = preg_replace('/牧唐-运营-/','',$v['username']);
            }
            $a['list'][$k]['username'] = $name;
            $a['list'][$k]['yeji'] = number_format($v['yeji'],2);
        }
       
        return $a;
    }
    // 运营小组数据详情
    public function get_xz_infos($data){
        $t = $data['time'];
        // $days = date('t', strtotime($t));
        $days = date('d');
        $t2 = strtotime(date("Y-m-d",strtotime("-1 day")));
        $list = OperateGroup::select()->toArray();
        $n = count($list);
        $num = 0;
        $zxh = 0;
        $zrw = 0;
        $z_avg_xh = 0;
        $z_yesterday_xh = 0;
        foreach ($list as $k=>$v){
            $list[$k]['num'] = count(explode(',',$v['member']));
            $rw = $this->get_xz_rw($v['member'],$t);
            $xh = $this->get_xz_xh($v['member'],$t);
            $list[$k]['xh'] = $xh;
            $list[$k]['avg_xh'] = round($xh/$days,2);
            $yesterday_xh = $this->get_yesterday_xh($v['member'],$t2);
            $list[$k]['rw'] = $rw; 
            $list[$k]['yesterday_xh'] = $yesterday_xh;
            
            if($rw > 0 && $xh > 0){
                $list[$k]['yue_lv'] = round(($xh/$rw)*100,2);
            }else{
                $list[$k]['yue_lv'] = 0;
            }
            if($list[$k]['avg_xh'] > 0 ){
                $list[$k]['yesterday_lv'] = round(($yesterday_xh/$list[$k]['avg_xh'])*100,2);
            }else{
                $list[$k]['yesterday_lv'] = 0;
            }
            

            $num += $list[$k]['num'];
            $zxh += $list[$k]['xh'];
            $zrw += $list[$k]['rw'];
            $z_avg_xh += $list[$k]['avg_xh'];
            $z_yesterday_xh += $list[$k]['yesterday_xh'];
        }
        
        $list[$n]['name'] = '合计';
        $list[$n]['num'] = $num; 
        $list[$n]['xh'] = number_format($zxh,2);
        $list[$n]['rw'] = number_format($zrw,2);
        $list[$n]['yesterday_xh'] = number_format($z_yesterday_xh,2);
        $list[$n]['avg_xh'] = number_format($z_avg_xh,2);
        if($zxh > 0 && $zrw > 0){
            $list[$n]['yue_lv'] = round(($zxh/$zrw)*100,2);
        }else{
            $list[$n]['yue_lv'] = 0;
        }
        if($z_avg_xh > 0){
            $list[$n]['yesterday_lv'] = round(($z_yesterday_xh/$z_avg_xh)*100,2);
        }else{
            $list[$n]['yesterday_lv'] = 0;
        }
       
        return $list;
    }
    //获取小组的任务
    function get_xz_rw($ids,$tt,$type=0){
        $t = strtotime($tt);
        $w[] = ['time','=',$t];
        if($type == 1){
            $w[] = ['operate_id','=',$ids];
        }else{
            $w[] = ['operate_id','in',$ids];
        }
        $rw = $this->where($w)->sum('yeji');
        return $rw;
    }
    //获取小组的消耗
    function get_xz_xh($ids,$tt,$type=0){
        $t = strtotime($tt);
        $t2 = strtotime($tt." +1 month -1 day");
        $w[] = ['time','between',[$t,$t2]];
        if($type == 1){
            $w[] = ['operation_id','=',$ids];
        }else{
            $w[] = ['operation_id','in',$ids];
        }
        return ItemConsume::where($w)->sum('currency');
    }
    //获取前一天消耗
    function get_yesterday_xh($ids,$t,$type=0){
        $w[] = ['time','=',$t];
        if($type == 1){
            $w[] = ['operation_id','=',$ids];
        }else{
            $w[] = ['operation_id','in',$ids];
        }
        return ItemConsume::where($w)->sum('currency');
    }
    // 运营个人数据详情
    public function get_gr_infos($data){
        $t = $data['time'];
        // $days = date('t', strtotime($t));
        $days = date('d');
        $t2 = strtotime(date("Y-m-d",strtotime("-1 day")));
        $w[] = ['role_id','=','18'];
        $w[] = ['status','=','1'];
        $list = User::field('id,username')->where($w)->select()->toArray();
        foreach($list as $k=>$v){
            $list[$k]['name'] = $this->get_gr_xz($v['id']);
            $rw = $this->get_xz_rw($v['id'],$t,1);
            $xh = $this->get_xz_xh($v['id'],$t,1);
            $list[$k]['rw'] = number_format($rw,2);
            $list[$k]['xh'] = number_format($xh,2);
            $avg_xh = round($xh/$days,2);
            $list[$k]['avg_xh'] = number_format($avg_xh,2);
            $yesterday_xh = $this->get_yesterday_xh($v['id'],$t2,1);
            $list[$k]['yesterday_xh'] = number_format($yesterday_xh,2);
            if($rw > 0 && $xh > 0){
                $list[$k]['yue_lv'] = round(($xh/$rw)*100,2);
            }else{
                $list[$k]['yue_lv'] = 0;
            }
            if($avg_xh > 0 ){
                $list[$k]['yesterday_lv'] = round(($yesterday_xh/$avg_xh)*100,2);
            }else{
                $list[$k]['yesterday_lv'] = 0;
            }
        }
        return $list;
    }
    //获取小组名称
    function get_gr_xz($id){
        $w1[] = ['member','like','%'.$id.'%'];
        return OperateGroup::where($w1)->value('name');
    }
    //获取消耗参考
    function get_xh_ck_info($data){
        $tt = $data['time'];
        $w[] = ['role_id','in','3,18,20'];
        $w[] = ['status','=','1'];
        $ids = User::where($w)->column('id');
        $t = strtotime($tt);
        $t2 = strtotime($tt." +1 month -1 day");
        $w1[] = ['time','between',[$t,$t2]];
        $w1[] = ['operation_id','in',$ids];
        $xh = ItemConsume::where($w1)->sum('currency');
        $arr['xh'] = round($xh/10000,2);
        $arr['num'] = count($ids);
        // $arr['avg'] = $arr['xh']/$arr['num'];
        $arr['avg'] = round($arr['xh']/$arr['num'],2);
        return $arr;
    }





}