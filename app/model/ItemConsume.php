<?php
namespace app\model;
use think\Model;
class ItemConsume extends Model{
    //user 运营
    public function operate(){
        return $this->belongsTo(User::class, 'operation_id')->bind(['operate_name'=>'username']);
    }
    //acc 账户
    public function opaccount(){
        return $this->belongsTo(OpAccount::class, 'op_account_id')->bind(['opaccount_name'=>'name_id']);
    }
    
    //添加消耗
    public function get_add($data){
        $validate = \think\facade\Validate::rule([
            'items_id|项目名称'  => 'require',
            'op_account_id|账户名称'  => 'require',
            'time|消耗时间'  => 'require',
            'currency|消耗账户币'  => 'require|egt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'balance|昨日余额'  => 'require|egt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'proof_url|凭证上传'  => 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
           
            return true;
        }
    }
    
    //修改消耗
    public function get_save($data){
        $validate = \think\facade\Validate::rule([
            'time|消耗时间'  => 'require',
            'currency|消耗账户币'  => 'require|egt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'balance|昨日余额'  => 'require|egt:0|regex:/^[0-9]+(.[0-9]{1,3})?$/',
            'proof_url|凭证上传'  => 'require',
        ]);
        
        if(!$validate->check($data)){
            // 验证失败 输出错误信息
            return $validate->getError();
        }else{
             
            return true;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}