<?php


namespace app\model;

use think\Model;

class Bom extends Model
{
    public function grade()
    {

    }
    //设定时间戳
    protected $type = [
        'bomupdatetime' => 'timestamp',
        'bomlogintime' => 'timestamp',
    ];

    /*
    * 获取物品分类表
    */
    public function sort()
    {
        return $this->hasOne('Bomsort', 'sortid', 'bomclassify')->bind(['sort_name' => 'sortname']);
    }
}