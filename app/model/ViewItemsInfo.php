<?php
namespace app\model;

use Symfony\Component\VarDumper\Exception\ThrowingCasterException;
use think\Model;
class ViewItemsInfo extends Model{
    //月度统计列表
    public function consume_list($p){
        if(isset($p['op_account_id'])&&!empty($p['op_account_id'])){
            $wh[] = ['op_account_id','=',$p['op_account_id']];
        }
        if(isset($p['operate_id'])&&!empty($p['operate_id'])){
            $wh[] = ['operate_id','=',$p['operate_id']];
        }
        if(isset($p['media_id'])&&!empty($p['media_id'])){
            $wh[] = ['media_id','=',$p['media_id']];
        }
        
        $t = date("Y-m-01",time());
        $t1 = strtotime($t);
        $t2 = strtotime("$t +1 month -1 day");
        if(isset($p['time'])&&!empty($p['time'])){
            $array=explode('&', $p['time']);
            $t1 = strtotime($array[0]);
            $t2 = strtotime($array[1])+86399; 
        }
        //$wh[] = ['create_time','<=',$t2];
        // $wh[] = ['end_time','<=',$t2];
        //$wh[] = ['end_time','>=',$t1];
        //$wh[] = ['create_time','between',[$t1,$t2]];
        // $wh[] = ['end_time','between',[$t1,$t2]];

        $wh[] = ['create_time','<=',$t2];
        $wh[] = ['end_time','>=',$t1];
        // $wh[] = ['end_time','>=',$t1];
        $wh[] = ['total_money','>',0];
        // $arr['list'] =$this->where($wh)->page($p['page']*1,$p['limit']*1)->select();
        // $arr['cnt'] = $this->where($wh)->count('id');
        $arr['list'] =$this->where($wh)->select();
        $arr['cnt'] = $this->where($wh)->count('id');
        foreach ($arr['list'] as $k => $v) {
            $b = $this->get_total_consume($v['id'],$t1,$t2,$v['op_account_id']);
            if($b['count'] <= 0){
                unset($arr['list'][$k]);
                continue;
            }
                
            $arr['list'][$k]['total_consume'] = $b['count'];
            $arr['list'][$k]['consume_profit'] = $b['profit'];
            $a = $this->get_total_currency($v['id'],$t1,$t2);
            $arr['list'][$k]['total_currency'] = $a['count'];
            $arr['list'][$k]['profit'] = $a['profit'];
            $arr['list'][$k]['acc_refund'] = $this->get_total_opacc_refund($v['op_account_id'],$v['id'],$t1,$t2);
            
            $arr['list'][$k]['sell_rebates'] = $this->get_sell_profit($v['id'],$t1,$t2);
        }
        
        return $arr;

    }
    
    
    //获取总消耗（账户币）
    public function get_total_consume($item_id,$t1,$t2,$op_account_id){
      
        $wh[] = ['time','between',[$t1,$t2]];
        $wh[] = ['items_id','=',$item_id];
        $wh[] = ['op_account_id','=',$op_account_id];
        $arr['count'] = ItemConsume::where($wh)->sum('currency');
        $arr['profit'] = ItemConsume::where($wh)->sum('consume_profit');
        return $arr;
    }
    //获取总充值（账户币）
    public function get_total_currency($item_id,$t1,$t2){
        $wh[] = ['create_time','between',[$t1,$t2]];
        $wh[] = ['items_id','=',$item_id];
        $wh[] = ['status','=',1];
        $wh[] = ['type','=',2];
        $arr['count'] = Recharge::where($wh)->sum('currency'); //practical_currency
        $arr['profit'] = Recharge::where($wh)->sum('profit');
        return $arr;
    }
    
     //获取账户退款（账户币）
     function get_total_opacc_refund($op_account_id,$items_id,$t1,$t2){
        $w[] = ['op_account_id','=',$op_account_id];
        $w[] = ['items_id','=',$items_id];
        $w[] = ['type','=',4];
        $r_currency = Recharge::where($w)->sum('currency');
        return $r_currency;
    }
    
    
     //获取销售利润
     public function get_sell_profit($item_id,$t1,$t2){
        $wh[] = ['create_time','between',[$t1,$t2]];
        $wh[] = ['items_id','=',$item_id];
        $wh[] = ['status','=',1];
        $wh[] = ['type','=',2];
        // $list  = Recharge::field('currency,rebate,sell_rebates')->where($wh)->select()->toArray();
        // $profit = 0;
        // foreach ($list as $k=>$v){
        //     if($v['sell_rebates'] > 0){
        //         $profit += cny_profit($v['currency'],$v['rebate'],$v['sell_rebates']);
        //     }
            
        // }
        // $arr['profit'] = $profit;
        // return $arr;
        return Recharge::where($wh)->sum('sell_profit');
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}