<?php
namespace app\api\controller;
use app\model\OpAccount;
use app\model\Token;
use app\model\Recharge;
use app\model\ItemConsume;
class Kuaishou {
    // 获取已授权的再跑的账户
    public function acc(){
        $wh[] = ['acc_type','=',1];
        $wh[] = ['is_banned','=',1];
        $wh[] = ['status','=',1];
        $wh[] = ['is_accredit','=',1];
        $wh[] = ['jindu','=',5];
        $ids = OpAccount::where($wh)->column('id');
        if(!empty($ids)){
            return $ids;
        }else{
            return false;
        }
    }
    //获取所有的Token和账户id
    function get_acc_id_token($ids){
        $wh[] = ['acc_id','in',$ids];
        $wh[] = ['group','=','ksapi'];
        $list = Token::field('access_token,acc_id,advertiser_ids')->where($wh)->select();
        if(!empty($list)){
            return $list;
        }else{
            return false;
        }
    }
    //获取账户流水
    public function flows($token,$data){
        $headers = [
            'Access-Token:'.$token,
            'Content-Type:application/json',
        ];
        $url = 'https://ad.e.kuaishou.com/rest/openapi/v1/advertiser/fund/daily_flows/';
        
        $rs = json_decode(http_post($url,$data, false, $headers),true);
        
        return $rs;
    }
    // 添加消耗
    function add_acc_consume($data){
        $w[] = ['acc.name_id','=',$data['advertiser_id']];
        $rs = OpAccount::alias('acc')
        ->field('acc.id,acc.operate_id,i.id items_id,i.operate_type')
        ->join('items i','acc.id = i.op_account_id')
        ->where($w)->find();

        $r = Recharge::where('op_account_id',$rs['id'])->order('create_time desc')->find();

        $d['time'] = strtotime($data['date']);
        $d['currency'] = $data['daily_charge'];
        $d['balance'] = $data['balance'];
        $d['items_id'] = $rs['items_id'];
        $d['operation_id'] = $rs['operate_id'];
        $d['op_account_id'] = $rs['id'];
        $d['is_operate'] = $rs['operate_type'];
        $d['rebate'] = $r['rebate'];
        $d['channel_rebate'] = $r['channel_rebate'];
        $d['consume_profit'] =cny_profit($data['daily_charge'],$r['rebate'],$r['channel_rebate']);
        try {
            $w1[] = ['items_id','=',$d['items_id']];
            $w1[] = ['time','=',$d['time']];
            $r1 = ItemConsume::where($w1)->value('id');
           
            if(empty($r1)){
                ItemConsume::create($d);
                echo $data['advertiser_id'].'消耗添加成功';
            }else{
                ItemConsume::where($w1)->update($d);
                echo $data['advertiser_id'].'消耗更新成功';
            }
            
        } catch (\Throwable $th) {
            //throw $th;
            echo $data['advertiser_id'].'消耗添加失败。原因：'. $th->getMessage();
        }
        
        
    }

    //获取消耗
    public function acc_consume(){
        $ids = $this->acc();
        if($ids != false){
            $list = $this->get_acc_id_token($ids);
            if($list != false){
                $t = date("Y-m-d",time()-86399);
                $data['start_date'] = $t;
                $data['end_date'] = $t;
                foreach($list as $k=>$v){
                    $data['advertiser_id'] = $v['advertiser_ids'];
                    $d = $this->flows($v['access_token'],$data);
                    
                    if($d['code'] == 0){
                        $this->add_acc_consume($d['data']['details'][0]);
                    }else{
                        echo $d['message'];
                    }
                    
                }
            }else{
                echo '获取已授权账户的Token和账户id失败';
            }
        }else{
            echo '获取已授权账户失败';
        }
    }


    // 测试
    function ff(){
        dump('sds');
    }
























}