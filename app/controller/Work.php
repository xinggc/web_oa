<?php
namespace app\controller;
use think\facade\Db;
use think\facade\View;
use app\model\UserAccount;
use app\model\Claim;
use app\model\Channel;
use app\model\Customer;
use app\model\User;
use app\model\Recharge;
class Work{
    /* 
     * 工作台
     *  */
    
    public function index(){
        
        
        return view();
    }
    //添加报销明细
    public function add_baoxiao(){
        $Claim = new Claim();
        if(request()->post() && input('t') == 'json'){
            $data = input('post.');
            $data['user_id'] = session('uid');
            $data['user_name'] = session('info.username');
            $data['type'] = 1;
            $rs = $Claim->add_user_bx($data);
            if($rs === true){
                log_add(1,'添加报销操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            //获取员工账号列表
            $this->get_user_acc_list();
            return view();
        }
    }
    //修改报销明细
    public function save_baoxiao(){
        $Claim = new Claim();
        if(request()->post()){
            $data = input('post.');
            $data['user_id'] = session('uid');
            $data['user_name'] = session('info.username');
            $data['type'] = 1;
            $data['status'] = 1;
            $rs = $Claim->save_user_bx($data);
            if($rs === 1){
                log_add(2,'修改报销操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $id = input('get.id');
            $rs = $Claim->where('id',$id)->find();
            //获取员工账号列表
            $this->get_user_acc_list();
            
            View::assign('rs',$rs);
            return view();
        }
    }
    //获取员工账号列表
    function get_user_acc_list(){
        $UserAccount = new UserAccount;
        $acc_list = $UserAccount->list_user_acc_all();
        View::assign('acc_list',$acc_list);
    }
    
    //添加新收款账号
    public function add_user_account(){
        $UserAccount = new UserAccount;
        if(request()->post() && input('t') == 'json'){
            $data = input('post.');
            $data['uid'] = session('uid');
            $rs = $UserAccount->add_zfb($data);
            if($rs === true){
                log_add(1,'添加新账户操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            
            return view();
        }
    }
    
    //审批 待处理
    public function list_sp_pending(){
        $Claim = new Claim();
        if(request()->post() && input('t') == 'json'){
            $arr = $Claim->get_list_sp_pending(input('post.'));
            
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }
    }
    //审批 已处理
    public function list_sp_accomplish(){
        $Claim = new Claim();
        if(request()->post() && input('t') == 'json'){
            $arr = $Claim->get_list_sp_accomplish(input('post.'));
            
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }
    }
    
    //添加预支信息
    public function add_yuzhi(){
        $Claim = new Claim();
        if(request()->post() && input('t') == 'json'){
            $data = input('post.');
            $data['user_id'] = session('uid');
            $data['user_name'] = session('info.username');
            $data['type'] = 2;
            $data['employ_time'] = strtotime($data["employ_time"]);
            $rs = $Claim ->get_add_yuzhi($data);
            if($rs === true){
                log_add(1,'添加预支信息操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            //获取员工账号列表
            $this->get_user_acc_list();
            return view();
        }
    }
    //编辑预支
    public function save_yuzhi(){
        $Claim = new Claim();
        if(request()->post() && input('t') == 'json'){
            $data = input('post.');
            $data['user_id'] = session('uid');
            $data['user_name'] = session('info.username');
            $data['type'] = 2;
            $data['status'] = 1;
            $data['employ_time'] = strtotime($data["employ_time"]);
            $rs = $Claim ->get_save_yuzhi($data);
            if($rs === 1){
                log_add(2,'修改预支信息操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $rs = $Claim->where('id',input('get.id'))->find();
            
            View::assign('rs',$rs);
            //获取员工账号列表
            $this->get_user_acc_list();
            return view();
        }
    }
    
    
    //添加赔付信息
    public function add_paid(){
        $Channel = new Channel();
        $Customer = new Customer();
        $Recharge = new Recharge();
        if(request()->post() && input('t')=='json'){
            $rs = $Recharge->add_paid(input('post.'));
            if($rs === true){
                log_add(1,'添加赔付信息操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $num = get_numbers('PF');
            $Channel_list = $Channel->select();
            $customer_list = $Customer->select();
            //获取员工账号列表
            $user_list = User::select();
            
            View::assign('num',$num);
            View::assign('channel_list',$Channel_list);
            View::assign('customer_list',$customer_list);
            View::assign('user_list',$user_list);
            return view();
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}