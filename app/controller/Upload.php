<?php
namespace app\controller;
use OSS\OssClient;
use OSS\Core\OssException;
class Upload{
    
    public function index(){
        require_once '../vendor/aliyun-oss/autoload.php';
        if(request()->isPost()){
            $file = request()->file('file');
            // 上传到本地服务器
            $savename = \think\facade\Filesystem::disk('public')->putFile( 'imgs', $file);
            //$savename = \think\facade\Filesystem::putFile( 'imgs', $file);
            $temp = explode(".", $_FILES["file"]["name"]);
            $extension = end($temp);
            $object = date("YmdHis",time()).'_'.rand(10000, 99999).'.'.$extension;
            $u = $this->update_oss('../public/storage/'.$savename,$object); 
            if($u === true){
                //上传成功删除本地文件
                unlink('../public/storage/'.$savename);
                $u = "http://mt-imgs.oss-cn-hangzhou.aliyuncs.com/".$object;
                return json(['code'=>0,'msg'=>'','data'=>$u]);
            }else{
                return json(['code'=>1,'msg'=>$u]);
            }
            
            
        }
        
        
    }
   
    //layui 富文本图片上传
    public function layui_update(){
        require_once '../vendor/aliyun-oss/autoload.php';
        if(request()->isPost()){
            $file = request()->file('file');
            // 上传到本地服务器
            $savename = \think\facade\Filesystem::disk('public')->putFile( 'imgs', $file);
            //$savename = \think\facade\Filesystem::putFile( 'imgs', $file);
            $temp = explode(".", $_FILES["file"]["name"]);
            $extension = end($temp);
            $object = date("YmdHis",time()).'_'.rand(10000, 99999).'.'.$extension;
            $u = $this->update_oss('../public/storage/'.$savename,$object); 
            if($u === true){
                //上传成功删除本地文件
                unlink('../public/storage/'.$savename);
                $u = "http://mt-imgs.oss-cn-hangzhou.aliyuncs.com/".$object;
                return json(['code'=>0,'msg'=>'','data'=>['src'=>$u,'title'=>'']]);
            }else{
                return json(['code'=>1,'msg'=>$u]);
            }
            
            
        }
        
        
    }
    
    //上传阿里云
    function update_oss($url,$name){

        $url = str_replace("\\","/",$url);
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        $accessKeyId = "LTAI4GAwLqWWRBufpvdqumbP";
        $accessKeySecret = "yHSuPVrcG6n5Bfm70BvWYIq0YZfYLj";
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        $endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
        // 存储空间名称
        $bucket= "mt-imgs";
        // <yourObjectName>上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg
        $object = $name;
        $filePath = $url;
        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossClient->uploadFile($bucket, $object, $filePath);
            return true;
        } catch (OssException $e) {
            return $e->getMessage();
        }
    }
    
    
}