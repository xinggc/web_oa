<?php
declare (strict_types = 1);

namespace app\controller;

use think\Request;
use app\model\Customer;
use app\model\Platform;
use app\model\Channel;
use app\model\Recharge;
use app\model\OpAccount;
use app\model\User;
use app\model\Items;
use app\model\ViewItem;
use app\model\AccRefund;
use think\facade\View;
use think\facade\Db;
use think\Model;
use app\model\AccTransfer;
use app\model\OperateYeji;
class Media
{
    
    // 媒介

    /**
     * 客户管理 
     */
    public function customer(Customer $Customer)
    {
        
        $customer = new Customer();
        if(request()->isPost()&&input('t')=='json'){
            $arr = $customer->get_list(input('post.name'),input('post.'));
            return json(['code'=>0,'msg'=>'','count'=>$arr['con'],'data'=>$arr['list']]);
        }
        $list = $customer->get_list();
        return view();
    }
    
    //添加客户
    public function add_customer(){
        $customer = new Customer();
        $user = new User();
        if(request()->isPost()&&input('t')=='json'){
            $data = input('post.');
            $rs = $customer->get_add($data);
            if($rs === true){
                log_add(1);
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            //获取媒介人员列表
            $list = $user->get_role_list('14,19');
            View::assign('user_list',$list);
            return view();
        }
        
    }
    
    //修改客户
    public function save_customer(){
        $customer = new Customer();
        $user = new User();
        if(request()->isPost()&&input('t')=='json'){
            $data = input('post.');
            $rs = $customer->get_save($data);
            if($rs === 1){
                log_add(2);
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $id = input('get.id');
            $rs = $customer->where('id',$id)->find();
            //获取媒介人员列表
            $list = $user->get_role_list('14,19');
            
            View::assign('user_list',$list);
            View::assign('rs',$rs);
            return view();
        }
        
    }
    
    //客户打款列表
    public function recharge(){
        $recharge = new Recharge();
        if(request()->isPost()&&input('t')=='json'){
            $name = input("post.name");
            if($name){
                $wh[] = ['cu.name','like','%'.$name.'%'];
            }
            if(session('info.is_leader') != 1 && session('info.role_id') !=1){
                $wh[] = ['re.media_id','=',session('uid')];
            } 
            $wh[] = ['re.type','=',1];
            $wh[] = ['re.is_paid','=',0];
            $wh[] = ['re.delete_time','=',null];
            $list = Db::name('recharge')->alias('re')
            ->field('re.id,re.code,re.is_advance,re.reject,re.amount,re.screenshot_url,re.create_time,re.status,re.note,u.username,uu.username audit_name,cu.name')
            ->leftJoin('user u','u.id = re.media_id')
            ->leftJoin('customer cu','cu.id = re.customer_id')
            ->leftJoin('user uu','uu.id = re.audit_id')
            ->where($wh)->order('re.update_time desc,id desc')->page(input('post.page')*1,input('post.limit')*1)->select()->toArray();
            foreach ($list as $k=>$v){
                $list[$k]["create_time"] = date('Y-m-d H:i',$v['create_time']);
            }
            $cnt = Db::name('recharge')->alias('re')->join('customer cu','cu.id = re.customer_id')->where($wh)->count('re.id');
            return json(['code'=>0,'msg'=>'','count'=>$cnt,'data'=>$list]);
        }else{
            return view();
        }
        
    }
    /**
     * 客户打款
     */
    public function add_recharge()
    {
        $recharge = new Recharge();
        if(request()->isPost()&&input('t')=='json'){
            $data = input('post.');
            $data['type'] = 1;
            $data['create_time'] = strtotime($data['create_time']);
            $rs = $recharge->get_cz_add($data);
            if($rs === true){
                log_add(1,'客户打款');
                //发送消息
                $r = Customer::find($data['customer_id']);
                get_msg_info(1,13,$r['name'],$data['amount']);
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
          
            $customer = Db::name('customer')->field('id,name')->select();
            //生成充值编号
            $num = get_numbers('CZ');
            //获取公司账号
            $this->get_account();
            //获取媒介人员
            $w[] = ['role_id','in','14,19'];
            $user_list = User::field('id,username')->where($w)->select();
           
            View::assign('user_list',$user_list);
            View::assign('customer',$customer);
            View::assign('num',$num);
            return view();
        }
        
    }
    //修改客户打款
    public function save_recharge(){
        $recharge = new Recharge();
        if(request()->isPost() && input('t') == 'json'){
            $data = input('post.');
            $data['status'] = 0;
            $data['create_time'] = strtotime($data['create_time']);
            $data['update_time'] = time();
            $rs = $recharge->get_cz_save($data);
            if($rs === 1){
                log_add(2,'修改客户打款');
                //发送消息
                $r = Customer::find($data['customer_id']);
                get_msg_info(1,13,$r['name'],$data['amount']);
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $id = input('get.id');
            $rs = Db::name("recharge")->where('id',$id)->find();
            $customer = Db::name('customer')->field('id,name')->select();
            //获取公司账号
            $this->get_account();
            //获取媒介人员
            $w[] = ['role_id','in','14,19'];
            $user_list = User::field('id,username')->where($w)->select();
             
            View::assign('user_list',$user_list);
            View::assign('customer',$customer);
            View::assign('rs',$rs);
            return view();
        }
        
    }
    
    //获取公司账号
    public function get_account(){
        $list = Db::name('account')->where('status',1)->select();
        View::assign('account_list',$list);
    }
    //删除客户打款
    public function cu_dk_del(){
        if(request()->isPost() && input('t')=='json'){
           try {
                Recharge::destroy(input('post.id'));
                log_add(3,'删除客户打款');
                return json(['code'=>1,'msg'=>'删除成功']);
           } catch (\Throwable $th) {
               //throw $th;
               return json(['code'=>0,'msg'=>$th->getMessage()]);
           }
        }
    }
    
    
    
    /**
     * 账户充值
     */
   
    //账户充值列表
    public function list_acc_recharge(){
        $recharge = new Recharge();
        $OpAccount = new OpAccount();
        if(request()->isPost()&&input('t')=='json'){
            $list = $recharge->get_acc_pay_list(input('post.name_id'),2,input('post.'));
            $cnt = $recharge->where('type',2)->count('id');
            return json(['code'=>0,'msg'=>'','count'=>$cnt,'data'=>$list]);
        }else{
            $wh[] = ['role_id','in','14,19'];
            $wh[] = ['status','=','1'];
            $media_list = User::field('id,username')->where($wh)->select();
            $acc_list = $OpAccount->field('id,name_id')->select(); 
            $customer_list = Customer::field('id,name')->select();
            View::assign('acc_list',$acc_list);
            View::assign('customer_list',$customer_list);
            View::assign('media_list',$media_list);
            return view();
        }
    }
    
   
    public function acc_recharges(){
        $OpAccount = new OpAccount();
        $Channel = new Channel();
        $recharge = new Recharge();
        Db::startTrans();
        if(request()->isPost() && input('t')=='json'){
            $data = input('post.');
            $data["media_id"] = $data["media_id"];
            $data["type"] = 2;
            $data["create_time"] = strtotime((string)$data["create_time"]);
            $data["update_time"] = $data["create_time"];
            $data["practical_currency"] = $data['currency'];
            $rs = $recharge->get_acc_add($data);
            if($rs !== true){
                return json(['code'=>0,'msg'=>$rs]);
            } 
            //返点 公司利润
            if($data['currency'] > 0){ 
                $data['profit'] = cny_profit($data['currency'],$data['rebate'],$data['channel_rebate']);
                if($data['profit'] < 0 ){
                    $data['profit'] = 0;
                }
            }
            
            //渠道金额
            $channel_money =$data['currency']/(1+$data['channel_rebate']/100);
            $data['channel_money'] = round($channel_money,2);
            //销售利润
            if($data['sell_rebates'] > 0){
                $data['sell_profit'] = cny_profit($data['currency'],$data['rebate'],$data['sell_rebates']);
            }
            
            
            try {
                
                Db::name('recharge')->save($data);
                // 提交事务
                Db::commit();
                log_add(1,'账户充值');
                //发送消息
                $opa = OpAccount::find($data['op_account_id']);
                get_msg_info(2,13,$opa['name_id'],$data['currency']);
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return json(['code'=>0,'msg'=>$e->getMessage()]);
            }
            
            
        }else{
            //获取账户充值编号
            $num = get_numbers('ACC');
            //获取账户ＩＤ
            $ids = $OpAccount->get_occupy_acc();
            $w[] = ['id','in',$ids];
            $OpAccount_list = $OpAccount->field('id,name_id')->where($w)->select();
            $w1[] = ['role_id','in','19,14'];
            $w1[] = ['status','=',1];
            $user_list = User::field('id,username')->where($w1)->select();
           
            View::assign('num',$num);
            View::assign('user_list',$user_list);
            View::assign('OpAccount_list',$OpAccount_list);
            return view();
        }
    }
    
    // 修改账户充值
    public function save_acc_recharges(){
        $OpAccount = new OpAccount();
        $Channel = new Channel();
        $recharge = new Recharge();
        if(request()->isPost() && input('t') == 'json'){
            $data = input('post.');
            $data["status"] = 0;
            $data["create_time"] = strtotime($data['create_time']);
            $data["update_time"] = time();
            $rs = $recharge->get_acc_save($data);
            if($rs !== true){
                return json(['code'=>0,'msg'=>$rs]);
            } 
            //返点 公司利润
            if($data['currency'] > 0){
                $data['profit'] = cny_profit($data['currency'],$data['rebate'],$data['channel_rebate']);
                if($data['profit'] < 0 ){
                    $data['profit'] = 0;
                }
            }
            
            //渠道金额
            $channel_money =$data['currency']/(1+$data['channel_rebate']/100);
            $data['channel_money'] = round($channel_money,2);
            //销售利润
            if($data['sell_rebates'] > 0){
                $data['sell_profit'] = cny_profit($data['currency'],$data['rebate'],$data['sell_rebates']);
            }else{
                $data['sell_profit'] = 0;
            }
            
            try {
                Db::name('recharge')->update($data);
                // 提交事务
                Db::commit();
                log_add(2,'修改账户充值');
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return json(['code'=>0,'msg'=>$e->getMessage()]);
            }
        }else{
            $id = input('get.id');
            $rs = Recharge::find($id);
             //获取账户ＩＤ
             $w['jindu'] = 5;
             $w['status'] = 1;
             $OpAccount_list = OpAccount::field('id,name_id')->where($w)->select();
             $w1[] = ['role_id','in','19,14'];
             $w1[] = ['status','=',1];
             $user_list = User::field('id,username')->where($w1)->select();

            View::assign('rs',$rs);
            View::assign('user_list',$user_list);
            View::assign('OpAccount_list',$OpAccount_list);
            return view();
        }
    }

    // 账户转账
    public function list_transfer(AccTransfer $AccTransfer){
        if(request()->isPost() && input('t') == 'json'){
            $arr = $AccTransfer->get_list(input('post.'));
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }
        $w['jindu'] = 5;
        $w['status'] = 1;
        $w['is_banned'] = 1;
        $acc_list = OpAccount::field('id,name_id')->where($w)->select();
        
        $wh[] = ['role_id','in','19,14'];
        $media_list = User::field('id,username')->where($wh)->select();
        View::assign('media_list',$media_list);
        View::assign('acc_list',$acc_list);
        return View();
    }
    //添加账户转账
    public function acc_transfer(AccTransfer $AccTransfer){
        if(request()->isPost() && input('t') == 'json'){
            $r = $AccTransfer->get_add(input('post.'));
           
            if($r == 1){
                log_add(1,'添加账户转账');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$r]);
            }
        }else{
            $w1[] = ['role_id','in','19,14'];
            $user_list = User::field('id,username')->where($w1)->select();
            $w['jindu'] = 5;
            $w['status'] = 1;
            $w['is_banned'] = 1;
            $acc_list = OpAccount::field('id,name_id')->where($w)->select();
            $ch_list = Channel::where('status',1)->select();
            $cu_list = Customer::field('id,name')->where('status',1)->select();

            View::assign('user_list',$user_list);
            View::assign('acc_list',$acc_list);
            View::assign('ch_list',$ch_list);
            View::assign('cu_list',$cu_list);
            return View();
        }

    }
    //修改账户转账
    public function save_acc_transfer(AccTransfer $AccTransfer){
        if(request()->isPost() && input('t') == 'json'){
            $r = $AccTransfer->get_save(input('post.'));
            if($r == 1){
                log_add(2,'修改账户转账');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$r?$r:'提交失败']);
            }
        }else{
            $rs = $AccTransfer->where('id',input('get.id'))->find()->toArray();
            $rs['create_time'] = date("Y-m-d",strtotime($rs['create_time']));
            
            $w1[] = ['role_id','in','19,14'];
            $user_list = User::field('id,username')->where($w1)->select();
            $w['jindu'] = 5;
            $w['status'] = 1;
            $w['is_banned'] = 1;
            $acc_list = OpAccount::field('id,name_id')->where($w)->select();
            $ch_list = Channel::where('status',1)->select();
            $cu_list = Customer::field('id,name')->where('status',1)->select();

            View::assign('rs',$rs);
            View::assign('user_list',$user_list);
            View::assign('acc_list',$acc_list);
            View::assign('ch_list',$ch_list);
            View::assign('cu_list',$cu_list);
            return View();
        }
    }
    //删除转账
    public function del_transfer(){
        if(request()->isPost() && input('t')=='json'){
            try {
                AccTransfer::destroy(input('post.id'));
                log_add(3,'删除账户转账');
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (\Throwable $th) {
                //throw $th;
                return json(['code'=>0,'msg'=>$th->getMessage()]);
            }
        }
    }

    //获取账户对应的信息
    public function get_OpAccount_info(){
        if(request()->isPost()&&input('t')=='json'){
            $acc_id = input('post.acc_id');
            $wh[] = ['acc_id','=',$acc_id];
            $wh[] = ['is_finish','=',0];
            $wh[] = ['status','=',1];
            $rs = ViewItem::where($wh)->find();
            if($rs){
                return json(['code'=>1,'msg'=>'提交成功','data'=>$rs]);
            }else{
                return json(['code'=>0,'msg'=>'当前账户未绑定项目']);
            }
        }
    
    }
    
    //计算利润
    function get_profit($data){
        //当前项目的渠道返点
        $item = Items::find($data['items_id']);
        //服务费
        if($data['service_pay']>0){
            if($item['channel_rebate']>=0){
                $l1 = $data['currency']/(1+$item['channel_rebate']/100);
            }
            $l2 = $data['currency']*(1+$data['service_pay']/100);
            $l = $l2-$l1;
            return $profit = round($l,2);
        }
        //客户返点
        if($data['rebate']>0 && $item['channel_rebate']>=0){
            $l1 = $data['currency']/(1+$item['channel_rebate']/100);
            $l2 = $data['currency']/(1+$data['rebate']/100);
            $profit = $l2-$l1;
            return $profit = round($profit,2);
        }
        return 0;
    }
    
    
    
    
    
    
    
    //删除账户充值
    public function acc_re_del(){
        if(request()->isPost()&& input('t')=='json'){
            $rs = Recharge::destroy(input('post.id'));
            if($rs){
                log_add(3,'删除账户充值操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>1,'msg'=>'删除失败']);
            }
        }
    }
    //账户退款
    public function get_acc_refund(){
        if(request()->isPost()&& input('t')=='json'){
            if(!preg_match("/^[0-9]+(.[0-9]{1,3})?$/",input('post.r_currency'))){
                return json(['code' => 0, 'msg' => '请输入有效的账户币']);
            }
            $r = Recharge::find(input('post.pid'));
            if(input('post.r_currency')<0 || input('post.r_currency') > ($r['currency'] - $r['refund'])){
                return json(['code' => 0, 'msg' => '请输入有效的账号币']);
            }
            if(empty(input('post.media_id'))){
                return json(['code' => 0, 'msg' => '媒介不能为空']);
            }
            if(empty(input('post.media_refund_url'))){
                return json(['code' => 0, 'msg' => '凭证不能为空']);
            }
            $da = input('post.');
            $data['pid'] = $da['pid'];
            $data['type'] = 4;
            $data['status'] = 0;
            $data['currency'] = $da['r_currency'];
            $data['amount'] = zhb_cny($da['r_currency'],$r['rebate']);
            $data['rebate'] = $r['rebate'];
            $data['channel_rebate'] = $r['channel_rebate'];
            $data['channel_money'] = zhb_cny($da['r_currency'],$r['channel_rebate']);
            $data['profit'] = cny_profit($da['r_currency'],$r['rebate'],$r['channel_rebate']);
            $data['sell_rebates'] = $r['sell_rebates'];
            if($r['sell_rebates'] > 0){
                $data['sell_profit'] = cny_profit($da['r_currency'],$r['rebate'],$r['sell_rebates']);
            }else{
                $data['sell_profit'] = 0;
            }
            
            $data['note'] = $da['note'];
            $data['items_id'] = $r['items_id'];
            $data['op_account_id'] = $r['op_account_id'];
            $data['media_id'] = $da['media_id'];
            $data['customer_id'] = $r['customer_id'];
            $data['channel_id'] = $r['channel_id'];
            $data['code'] = get_numbers('TK');
            $data['screenshot_url'] = $da['media_refund_url'];
            $data['create_time'] = time();
            $data['update_time'] = time();
            Db::startTrans();
            try {
                Db::name('recharge')->strict(false)->save($data);
                // 提交事务
                Db::commit();
                log_add(1, '媒介账户退款');
                return json(['code' => 1, 'msg' => '提交成功']);
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return json(['code' => 0, 'msg' => $e->getMessage()]);
            }
        }else{
            $w1[] = ['role_id','in','19,14'];
            $user_list = User::field('id,username')->where($w1)->select();

            View::assign('user_list',$user_list);
            return view();
        }
    }
    
    
    
    
    //获取渠道对应的账户
    public function get_acc_info(){
        $OpAccount = new OpAccount();
        if(request()->isPost()&&input('t')=='json'){
            $channel = input('post.channel_id');
            if(empty($channel)){
                return json(['code'=>0,'msg'=>'当前渠道未绑定账户']);
            }
            $list = $OpAccount->get_acc_list(0,$channel);
            if($list){
                return json(['code'=>1,'msg'=>'提交成功','data'=>$list]);
            }else{
                return json(['code'=>0,'msg'=>'当前渠道未绑定账户']);
            }
        }
    }
    //获取账户id对应的客户信息
    public function get_cu_acc_info(){
        $customer = new Customer();
        if(request()->isPost()&&input('t')=='json'){
            $id = input('post.acc_id');
            $wh[] = ['acc_id','=',$id];
            $rs = ViewItem::where($wh)->find();
            if($rs){
                return json(['code'=>1,'msg'=>'提交成功','data'=>$rs]);
            }else{
                return json(['code'=>0,'msg'=>'当前账户未绑定项目']);
            }
        }
        
    }

    
    

    /**
     * 渠道管理
     */
    public function channel()
    {
        $Channel = new Channel();
        if(request()->isPost()&&input('t')=='json'){
            $list = $Channel->get_list(input('post.'));
            $cnt = $Channel->count('id');
            return json(['code'=>0,'msg'=>'','count'=>$cnt,'data'=>$list]);
        }else{
            return view('');
        }
        
    }
    
    //修改渠道状态
    public function get_save_channel_status(){ 
        $Channel = new Channel();
        if(request()->isPost()&&input('t')=='json'){
            $data = input('post.');
            if($data['status'] == 1){
                $pid = Channel::where("id",$data['id'])->value('pid');
                $sta = Platform::where('id',$pid)->value("status");
                if($sta == 0){
                    return json(['code'=>0,'msg'=>'该渠道的平台未开启，请先开启平台']);
                }
            }
            $rs = Channel::update($data);
            if($rs){
                log_add(2,'修改渠道状态');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>'提交失败']);
            }
        }
    }
    //添加渠道
    public function add_channel(){
        $Platform = new Platform();
        $Channel = new Channel();
        if(request()->isPost()&&input('t')=='json'){
            $rs = $Channel->get_add(input('post.'));
            if($rs === true){
                log_add(1,'添加渠道');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $pt_list = $Platform->select();
            View::assign("pt_list",$pt_list);
            return view();
        }
        
    }
    //修改渠道
    public function save_channel(){
        $Channel = new Channel();
        $Platform = new Platform();
        if(request()->isPost()&&input('t')=='json'){
            $data = request()->post();
            $rs = $Channel->get_save($data);
            if($rs === 1){
                log_add(2,'修改渠道');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $id = input('get.id');
            $rs = $Channel->where('id',$id)->find();
            $pt_list = $Platform->select();
            
            View::assign("pt_list",$pt_list);
            View::assign('rs',$rs);
            return view();
        }
    }
    
    /**
     * 平台管理
     */
    public function platform()
    {
        $Platform = new Platform();
        if(request()->isPost()&&input('t')=='json'){
            $list = $Platform->get_list(input('post.'));
            return json(['code'=>0,'msg'=>'','count'=>count($list),'data'=>$list]);
        }else{
            return view('');
        }
       
    }
    
    //修改平台状态
    public function get_save_platform_status(){
        $Platform = new Platform();
        if(request()->isPost()&&input('t')=='json'){
            $data = input('post.');
            Db::startTrans();
            try {
                $Platform->where('id',$data['id'])->save($data);
                Channel::where('pid',$data['id'])->save(['status'=>$data['status']]);
                // 提交事务
                Db::commit();
                log_add(2,'修改平台状态');
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return json(['code'=>0,'msg'=>$e->getMessage()]);
            }
            
        }
    }
    //添加平台
    public function add_platform(){
        $Platform = new Platform();
        if(request()->isPost()&&input('t')=='json'){
            $data = request()->post();
            $rs = $Platform->get_add($data);
            if($rs === true){
                log_add(1,'添加平台');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            
            return view();   
        }
    }
    //修改平台信息
    public function save_platform(){
        $Platform = new Platform();
        if(request()->isPost()&&input('t')=='json'){
            $data = request()->post();
            $rs = $Platform->get_save($data);
            if($rs === 1){
                log_add(2,'修改平台信息');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $id = input('get.id');
            $rs = $Platform->where('id',$id)->find();
            View::assign('rs',$rs);
            return view();
        }
        
    }
    
    
    
    //快捷充值
    public function top_up(){
        $recharge = new Recharge();
        if(request()->isPost() && input('t') == 'json'){
            $data = input('post.');
            $rs = $recharge->get_kj_cz($data);
            if($rs === true){
                // 启动事务
                Db::startTrans();
                try {
                   
                    //添加客户打款
                    if(!empty($data['amount'])){
                        $d1['amount'] = $data['amount'];
                        $d1['customer_id'] = $data['customer_id'];
                        $d1['media_id'] = $data['media_id'];
                        $d1['code'] = get_numbers('CZ');
                        $d1['screenshot_url'] = $data['screenshot_url'];
                        $d1['note'] = $data['note'];
                        $d1['create_time'] = strtotime($data['create_time']);
                        $d1['update_time'] = time();
                        $d1['type'] = 1;
                        $d1['is_advance'] = $data['is_advance'];
                        Db::name('recharge')->save($d1);
                    }
                    //添加账户充值
                    if(isset($data['currency']) && count($data['currency']) > 0){
                        foreach ($data['currency'] as $k=>$v){
                            $arr['currency'] = $data['currency'][$k];
                            $acc = $this->get_acc_acc($data['name_id'][$k]);
                            $arr['op_account_id'] = $acc['id'];
                            $arr['channel_id'] = $acc['channel_id'];
                            $arr['customer_id'] = $data['customer_id'];
                            $arr['media_id'] = $data['media_id'];
                            $arr['code'] = get_numbers('ACC');
                            $arr['rebate'] = $data['rebate'][$k];
                            $arr['amount'] = $data['amounts'][$k];
                            $arr['note'] = $data['note'];
                            $arr['create_time'] = strtotime($data['create_time']);
                            $arr['update_time'] = time();
                            $arr['type'] = 2;
                            $arr['items_id'] = $this->get_item_item($acc['id']);
                            $arr['channel_rebate'] = $data['channel_rebate'][$k];
                            $arr['channel_money'] =round($arr['currency']/(1+$arr['channel_rebate']/100),2); 
                            $arr['sell_rebates'] = $data['sell_rebates'][$k];
                            $arr['practical_currency'] = $data['currency'][$k];
                            $arr['profit'] = cny_profit($arr['currency'],$arr['rebate'],$arr['channel_rebate']); //利润
                             //销售利润
                            if($arr['sell_rebates'] > 0 ){
                                $arr['sell_profit'] = cny_profit($arr['currency'],$arr['rebate'],$arr['sell_rebates']);
                            }
                            
                            $da[] = $arr;
                           
                        }
                        Db::name('recharge')->insertAll($da);
                    }
                    //return json(['code'=>0,'msg'=>$d1,'msg2'=>$da]);
                    
                    // 提交事务
                    Db::commit();
                    //发送消息
                    get_msg_info(2,13,'多个账户充值','***');
                    return json(['code'=>1,'msg'=>'提交成功']);
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                    return json(['code'=>0,'msg'=>$e->getMessage()]);
                } 
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $cu_id = input('get.id');
            $rs = Customer::where('id',$cu_id)->find();
            $w1[] = ['role_id','in','19,14'];
            $user_list = User::field('id,username')->where($w1)->select();
            
            View::assign('rs',$rs);
            View::assign('user_list',$user_list);
            return view();
        }
    }
    
    public function get_acc_acc($name_id){
        $w[] = ['name_id','=',$name_id];
        $acc = OpAccount::field('id,channel_id')->where($w)->find();
        return $acc;
    }
    public function get_item_item($acc){
        $w1[] = ['is_deliver','=',1];
        $w1[] = ['is_finish','=',0];
        $w1[] = ['op_account_id','=',$acc];
        $items_id = Items::where($w1)->value('id');
        return $items_id;
    }
    //获取客户下的所有账户
    public function get_acc(){
        if(request()->isPost() && input('t')=='json'){
            $cu_id = input('post.cu_id');
            $w[] = ['is_deliver','=',1];
            $w[] = ['is_finish','=',0];
            $w[] = ['customer_id','=',$cu_id];
            $acc_ids = Items::where($w)->column('op_account_id');
            $wh[] = array('id','in',$acc_ids);
            $wh[] =['status','=',1];
            $list = OpAccount::field('id value,name_id title')->where($wh)->select();
            if(count($list) > 0){
                return json(['code'=>1,'msg'=>'提交成功','data'=>$list]);
            }else{
                return json(['code'=>0,'msg'=>'当前账户未绑定项目']);
            }
        }
        
    }
    
    //快捷充值获取端口
    public function get_channel_info(){
        if(request()->isPost() && input('t')=='json'){
            $acc = input('post.acc');
            try {
                $r = ViewItem::where('name_id',$acc)->find();
                return json(['code'=>1,'msg'=>'提交成功','data'=>$r]);
            } catch (\Throwable $th) {
                //throw $th;
                return json(['code'=>0,'msg'=>$th->getMessage()]);
            }
            
        }
    }
    
    // 任务设置
    public function task(){
        if(request()->isPost() && input('t')=='json'){
            $data = input("post.");
            if(isset($data['media_id']) && !empty($data['media_id'])){
                $w[] = ['rw.media_id','=',$data['media_id']];
            }else{
                $w[] = ['rw.media_id','>',0];
            }
            if(isset($data['time']) && !empty($data['time'])){
                $t = explode('&',$data['time']);
                $t1 = strtotime($t[0]);
                $t2 = strtotime($t[1]);
                $w[] = ['rw.time','between',[$t1,$t2]];
            }else{
                $t = strtotime(date('Y-m',time()));
                $w[] = ['rw.time','=',$t];
            }
            
            
            $w[] = ['u.status','=','1'];
            $list = OperateYeji::alias('rw')
            ->field('u.username,u.role_id, rw.*')
            ->join('user u','rw.media_id = u.id','LEFT')
            ->where($w)->select()->toArray();
            foreach($list as $k=>$v){
                $sell = $this->get_wanc_rw($v);
                $list[$k]['sell'] = $sell;
                $list[$k]['lv'] = round(($list[$k]['sell']/$v['yeji'])*100,2) ;
                $list[$k]['yeji'] = number_format((float)$v['yeji'],2);
                
            }
            return json(['code'=>0,'msg'=>'','data'=>$list]);
        }
       
        $w1[] = ['role_id','in','19,14'];
        $w1[] = ['status','=','1'];
        $user_list = User::field('id,username')->where($w1)->select();

        View::assign('user_list',$user_list);
        return view();
    }
    // 获取当前月已完成的任务
    function get_wanc_rw($v){
        $t = strtotime(date('Y-m-01',time()));
        $t2 = strtotime(date("Y-m-d",$t).' +1 month -1 day');
        $w1[] = ['role_id','in','19,14'];
        $w1[] = ['status','=','1'];
        $ids = User::where($w1)->column('id');
        if($v['role_id'] == 14){
            $w2[] = ['create_time','between',[$t,$t2]];
            $w2[] = ['type','=',2];
            $w2[] = ['status','=',1];
            $w2[] = ['media_id','in',$ids];
            $sell2 = Recharge::where($w2)->sum('sell_profit');
            $w3[] = ['create_time','between',[$t,$t2]];
            $w3[] = ['type','=',4];
            $w3[] = ['status','=',1];
            $w3[] = ['media_id','in',$ids];
            $sell3 = Recharge::where($w3)->sum('sell_profit');
        }else{
            $w2[] = ['create_time','between',[$t,$t2]];
            $w2[] = ['type','=',2];
            $w2[] = ['status','=',1];
            $w2[] = ['media_id','=',$v['media_id']];
            $sell2 = Recharge::where($w2)->sum('sell_profit');
            $w3[] = ['create_time','between',[$t,$t2]];
            $w3[] = ['type','=',4];
            $w3[] = ['status','=',1];
            $w3[] = ['media_id','=',$v['media_id']];
            $sell3 = Recharge::where($w3)->sum('sell_profit');
        }
       
      
        $sell = $sell2 - $sell3;
        $sell = round($sell,2);
       
        return $sell;
    }
    //添加任务
    public function add_task(OperateYeji $OperateYeji){
        if(request()->isPost() && input('t')=='json'){
            $rs = $OperateYeji->get_add_task(input('post.'));
            if($rs === 1){
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>1,'msg'=>$rs]);
            }
        }
        $w1[] = ['role_id','in','19,14'];
        $w1[] = ['status','=','1'];
        $user_list = User::field('id,username')->where($w1)->select();

        View::assign('user_list',$user_list);
        return view();
    }
    //修改任务
    public function save_task(OperateYeji $OperateYeji){
        if(request()->isPost() && input('t')=='json'){
            $rs = $OperateYeji->get_save_task(input('post.'));
            if($rs === 1){
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>1,'msg'=>$rs]);
            }
        }
        $rs = $OperateYeji->where('id',input('get.id'))->find();
        $w1[] = ['role_id','in','19,14'];
        $w1[] = ['status','=','1'];
        $user_list = User::field('id,username')->where($w1)->select();

        View::assign('user_list',$user_list);
        View::assign('rs',$rs);
        return view();
    }
    //任务删除
    public function task_del(){
        if(request()->isPost() && input('t')=='json'){
            try {
                OperateYeji::destroy(input('post.id'));
                log_add(3,'删除任务');
                return json(['code' => 1, 'msg' => '提交成功']);
            } catch (\Throwable $th) {
                //throw $th;
                return json(['code' => 0, 'msg' => $th->getMessage()]);
            }
        }
    }

    // 客户打款，充值明细
    public function cu_fund_inmfo(recharge $recharge){
        if(request()->isPost() && input('t')=='json'){ 
            $list = $recharge->get_cu_fund_info(input('post.id'));
            return json(['code'=>0,'msg'=>'提交成功','data'=>$list]);
        }
        
        return view();
    }

    
}
