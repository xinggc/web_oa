<?php
namespace app\controller;
use app\model\WxGroup;
use app\model\User;
use think\facade\Db;
use think\facade\View;
use app\model\Channel;
class WxGroups{

    public function index(){
        $wxgroup = new WxGroup();
        if(request()->isPost() && input('t')=='json'){
            $list = $wxgroup->get_list();
            return json(['msg' => '', 'code' => 0,'data'=>$list]);
        }
        
        return view();
    }

    //添加群聊
    public function add_group(){
        $qywxapi = new QywxApi();
        if(request()->isPost() && input('t')=='json'){
            $rs = $qywxapi->add_qywx_group(input('post.'));
            if($rs === true){
                return json(['msg' => '提交成功', 'code' => 1]); 
            }else{
                return json(['msg' => $rs, 'code' => 0]);
            }
        }else{
            // $data['name'] = '测试-技术部';
            // $data['owner'] = '';
            // $data['userlist'] = 'xinggc,ZhangLongYi';
            // $rs = $qywxapi->add_qywx_group($data);
            // dump($rs);
            $user_list = User::alias('u')
            ->field('wx.userid id,u.username')
            ->join('user_qywx wx','wx.id = u.qywx_id','LEFT')
            ->where('u.status',1)->select();

            View::assign('user_list',$user_list);
            return view();
        }

    }
    //修改群聊
    public function save_group(){
        if(request()->isPost() && input('t')=='json'){
            try {
                $data = input('post.');
                WxGroup::update($data);
                return json(['msg' => '提交成功', 'code' => 1]); 
            } catch (\Throwable $th) {
                return json(['msg' => $th->getMessage(), 'code' => 0]);
            }
            
        }else{

            return view();
        }
    }

    //获取公司人员
    public function user_list(){
        if(request()->isPost() && input('t')=='json'){
            $id = input('post.id',0);
            
            try {
                $arr['u_list'] = User::alias('u')
                ->field('wx.userid value,u.username title')
                ->join('user_qywx wx','wx.id = u.qywx_id','LEFT')
                ->where('u.status',1)->select();

                $arr['ch_list'] = Channel::field('id value,name title')->where('status',1)->select()->toArray();
                if($id){
                    $channel_ids = WxGroup::where('id',$id)->value('channel_ids');
                    $channel_ids = explode(',',$channel_ids);
                    $arr['ch_list_checked'] = $channel_ids;
                }
                return json(['msg' => '', 'code' => 1,'data'=>$arr]);
            } catch (\Throwable $th) {
                //throw $th;
                return json(['msg' => $th->getMessage(),'code' => 0]);
            }
        }
    }



    //删除群聊
    public function del_group(){
        if(request()->isPost() && input('t')=='json'){
            try {
                WxGroup::destroy(input('post.id'));
                return json(['msg' => '提交成功', 'code' => 1]);
            } catch (\Throwable $th) {
                return json(['msg' => $th->getMessage(),'code' => 0]);
            }
            
        }
        

    }


























}





