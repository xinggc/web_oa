<?php
declare (strict_types = 1);
namespace app\controller;
use app\model\Items;
use app\model\Channel;
use app\model\Customer;
use app\model\OpAccount;
use app\model\Industry;
use app\model\AccItem;
use think\facade\View;
use app\model\ItemConsume;
use think\facade\Db;
use think\Model;
use app\model\User;
class Item{
    //项目
    public function index(){
        $ltems = new Items();
        if(request()->isPost() && input('t') == 'json'){
            $arr = $ltems->get_items_list(input('post.'));
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }else{
            return view();
        }   
    }
    
    //添加项目
    public function items_add(){
        $Channel = new Channel();
        $Customer = new Customer();
        $Items = new Items();
        $Industry = new Industry();
        $acc = new OpAccount();
        if(request()->isPost() && input('t') == 'json'){
            $rs = $Items->items_add(input('post.'));
            
            if($rs === true){
                log_add(1,'新建项目操作');
                return json(['code' => 1, 'msg' => '提交成功']);
            }else{
                return json(['code' => 0, 'msg' => $rs]);
            }
        }else{
            $channel_list = $Channel->select();
            $customer_list = $Customer->select();
            $industry_list = $Industry->select();
            $acc_list = $acc->get_acc_list(1);
            
            View::assign('channel_list',$channel_list);
            View::assign('customer_list',$customer_list);
            View::assign('industry_list',$industry_list);
            View::assign('acc_list',$acc_list);
            return view();
        }
    }
    
    //修改项目状态
    public function get_save_item_status(){
        $Items = new Items();
        if(request()->isPost() && input('t')=='json'){
            $deliver = $Items->where('id',input('post.id'))->value('is_deliver');
            if($deliver == 1){
                return json(['code' => 0, 'msg' => '当前项目在投递']);
            }
            $rs = $Items->where('id',input('post.id'))->save(input('post.'));
            if($rs){
                log_add(2,'修改项目状态操作');
                return json(['code' => 1, 'msg' => '提交成功']);
            }else{
                return json(['code' => 0, 'msg' => $rs]);
            }
        }
    }
    
    
    
   //删除项目
   public function item_del(){
       if(request()->isPost() && input('t')=='json'){
           $rs = Items::destroy(input('post.id'));
           if($rs){
               log_add(3,'软删除项目操作');
               return json(['code' => 1, 'msg' => '提交成功']);
           }else{
               return json(['code' => 0, 'msg' => $rs]);
           }
       }
   }
    //获取渠道返点
    public function get_channel_fd(){
        if(request()->isPost() && input('t')=='json'){
            $rebate = Channel::where('id',input('post.id'))->value('rebate');
            if($rebate){
                return json(['code' => 1, 'msg' => '','data'=>$rebate]);
            }else{
                return json(['code' => 0, 'msg' => '但前渠道未设置返点']);
            }
        }
    }
    
    //账户进度
    public function account_jindu(){
        $w[] = ['is_deliver','=',1];
        $w[] = ['is_finish','=',0];
        $acc_ids = Items::where($w)->column('op_account_id');
        $wh[] = ['id','not in',$acc_ids];
        $list = OpAccount::where($wh)->with('industry')->order('jindu desc')->select();
        
        View::assign('list',$list); 
        return view();
    }
    
    /* 
     * 运营项目管理
     *  */
    //项目列表
    public function yy_item_list(){
        
        $Items = new Items();
        $User = new User();
        if(request()->isPost() && input('t')=='json'){
            $arr = $Items->get_yy_item_list(input('post.'));
            
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }else{
            $account = OpAccount::where('status',1)->select();
            $User->dept_user_info(2);

            View::assign('account',$account);
            return view();
        }
        
    }
    
    
    
    //项目结束
    public function get_today_is_consume(){
        if(request()->isPost() && input('t') == 'json'){
            /* if(session('info.is_leader') != 1 && session('info.role_id') !=1 ){
                return json(['code' => 0, 'msg' => '您没有操作权限，请联系主管或管理员']);
            } */
            $rs = ItemConsume::where('items_id',input('post.items_id'))->whereTime('time', 'today')->find();
            //判断当前项目，在项目结束当天是否有添加消耗
            if(empty($rs)){
                return json(['code' => 0, 'msg' => '没有消耗数据']);
            }else{
                $data['id'] = input('post.items_id');
                $data['is_finish'] = 1;
                $data['is_deliver'] = 0;
                $data['end_time'] = time();
                $data['update_time'] = time();
                // 启动事务
                Db::startTrans();
                try {
                    Db::name('items')->save($data);
                    // 提交事务
                    Db::commit();
                    log_add(2,'运营给项目结束操作');
                    return json(['code' => 1, 'msg' => '项目结束成功']);
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                    return json(['code' => 0, 'msg' => $e->getMessage()]);
                }
            }
             
        }
    }
    
   
    /* 
     * 新增行业
     *  */
    public function industry_add(){
        if(request()->isPost()&&input('t')=='json'){
            $data = input('post.');
            $validate = \think\facade\Validate::rule([
                'name|行业名称' => 'require|unique:industry',
            ]);
            
            if (!$validate->check($data)) {
                // 验证失败 输出错误信息
                return json(['code' => 0, 'msg' => $validate->getError()]);
            } else {
                $rs = Db::name('industry')->save($data);
                if ($rs) {
                    return json(['code' => 1, 'msg' => '提交成功']);
                } else {
                    return json(['code' => 0, 'msg' => '提交失败']);
                }
            }
            
        }
    }
    
    
    //消耗列表
    public function consume_list(){ 
        if(request()->isPost() && input('t') == 'json'){
            $data = input('post.');
            $wh[] = ['items_id' , '=' , $data['items_id']];
            $wh[] = ['op_account_id','=',$data['op_account_id']];
            $list = ItemConsume::where($wh)->order('time desc')->page($data['page']*1,$data['limit']*1)->select();
            $con = ItemConsume::where($wh)->count("id");
            return json(['code'=>0,'msg'=>$data,'count'=>$con,'data'=>$list]);
        }else{
            return view();
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}