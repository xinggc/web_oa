<?php
namespace app\controller;
use app\model\Order;
use app\model\User;
use app\model\Customer;
use think\facade\View;
class Orders{
    public function index(){
        $Order = new Order();
        $Customer = new Customer();
        if(request()->isPost() && input('t')=='json'){
            $arr = $Order->get_list(input('post.'));
            
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }else{
            $Customer_list = $Customer->get_search_cu_list();
            View::assign('Customer_list',$Customer_list);
            return view();
        }
         
    }
    
    //添加订单
    public function add_order(){
        $Customer = new Customer();
        $order = new Order();
        if(request()->isPost() && input('t')== 'json'){
            $data = input('post.');
            $r = $order->get_add_order($data);
            if($r === true){
                log_add(1,'订单添加成功');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$r]);
            }
        }else{
            $wh[] = ["role_id",'in','19,14'];
            $media_list = User::field('id,username')->where($wh)->select();
            $Customer_list = $Customer->get_search_cu_list();
            $num = get_numbers('DD');
            
            
            View::assign('media_list',$media_list);
            View::assign('Customer_list',$Customer_list);
            View::assign('order_num',$num);
            return view();
        }
        
    }
    
    //修改订单
    public function save_order(){
        $Customer = new Customer();
        $order = new Order();
        if(request()->isPost() && input("t") == 'json'){
            $rs = $order->get_save_order(input('post.'));
            if($rs != false){
                log_add(2,'订单跟新成功');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }else{
            $id = input('get.id');
            $rs = Order::where('id',$id)->find();
            $wh[] = ["role_id",'in','19,14'];
            $media_list = User::field('id,username')->where($wh)->select();
            $Customer_list = $Customer->get_search_cu_list();
            
            View::assign('rs',$rs);
            View::assign('media_list',$media_list);
            View::assign('Customer_list',$Customer_list);
            return view();
        }
    }
    
    //删除订单
    public function del_order(){
        if(request()->isPost() && input("t")=='json'){
            $id = input("post.id");
            $rs = Order::destroy($id);
            if($rs){
                log_add(3,'订单跟删除成功');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}