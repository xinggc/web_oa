<?php
namespace app\controller;
use app\model\Order;
use app\model\Customer;
use think\facade\View;
use think\facade\Db;
use think\Model;
class Settlement{
    public function index(){
        $Order = new Order();
        $Customer = new Customer();
        if(request()->isPost() && input('t') == 'json'){
            $data = input('post.');
            $arr = $Order->get_list($data);
            
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
            
        }else{
            $wh[] = ['status','=',1];
            $Customer_list = $Customer->field('id,name')->where($wh)->select();
            View::assign('Customer_list',$Customer_list);
            return view();
        }
        
        
    }
    
    
    //结算订单 
    public function jiesuan(){
        $Order = new Order();
        if(request()->isPost() && input('t') == 'json'){
            $id = input('post.ids');
            try {
                $wh[] = ['id','=',$id];
                $Order->where($wh)->update(['is_jiesuan'=>2]);
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (Exception $e) {
                return json(['code'=>0,'msg'=>$e->getMessage()]);
            }
            
            
        }
        
    }
    //驳回
    public function reject(){
        if(request()->isPost() && input('t')=='json'){
            $data = input('post.');
            $data['is_jiesuan'] = 1;
            try {
                Order::update($data);
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (Exception $e) {
                return json(['code'=>0,'msg'=>$e->getMessage()]);
            }
            
        }
           
    }
    
    
    
    //销售统计
    public function sell_stat(){
        $order = new Order();
        $Customer = new Customer();
        if(request()->isPost() && input('t')=='json'){
            $arr = $order->get_sell_stat(input('post.'));
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }else{
            $Customer_list = $Customer->field('id,name')->select();
            View::assign('Customer_list',$Customer_list);
            return view();
        }
        
    }
    
    //销售统计详情
    public function sell_info(){
        
        if(request()->isPost() && input('t')=='json'){
            $data = input('post.');
            $wh[] = ['customer_id','=',$data['customer_id']];
            $t = strtotime(date("Y-m-1",$data['sell_time']));
            $t2 = date('Y-m-d', strtotime(date("Y-m-1",$data['sell_time']) ." +1 month -1 day"));
            $t2 = strtotime($t2);
            $s= [$t,$t2];
            $wh[] = ['sell_time','between',$s];
            $list = Order::where($wh)->with(['media','customer'])->page($data['page'],$data['limit'])->select();
            $cnt = Order::where($wh)->count('id');
            return json(['code'=>0,'msg'=>'','count'=>$cnt,'data'=>$list]);
        }else{
            
            return view();
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}