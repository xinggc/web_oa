<?php
namespace app\controller;
use think\facade\View;
use think\facade\Db;
use app\model\ReturnedMoney;
use app\model\Customer;
class ReturnMoney{
    //refund/index
    //垫款列表
    public function advance_list(ReturnedMoney $ReturnedMoney){
        $Customer = new Customer();
        if(request()->post() && input("t") == 'json'){
            $arr = $ReturnedMoney->get_advance_list(input('post.'));
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }else{
            //获取客户列表
            $cu_list = $Customer->get_search_cu_list();
            
            View::assign('cu_list',$cu_list);
            return view();
        }
        
    }
    
    
    //回款
    public function return_money(){
        if(request()->post() && input('t') == 'json'){
            $data = input('post.');
            $data['oper_name'] = session('info.username');
            $data['status'] = 2;
            $data['create_time'] = strtotime($data['create_time']);
            $data['update_time'] = strtotime($data['create_time']);
            if(empty($data['returned_money_url'])){
                return json(['code'=>0,'msg'=>'凭证不能为空']);
            }
            $rs = ReturnedMoney::update($data);
            if($rs){
                log_add(2,'媒介回款操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>'提交失败']);
            }
        }else{
            return view();
        }
        
    }
    
    //修改回款
    public function return_money_save(){
        if(request()->post() && input('t') == 'json'){
            $data = input('post.');
            $data['oper_name'] = session('info.username');
            $data['status'] = 2;
            if(empty($data['returned_money_url'])){
                return json(['code'=>0,'msg'=>'凭证不能为空']);
            }
            $rs = ReturnedMoney::update($data);
            if($rs){
                log_add(2,'媒介修改回款操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>'提交失败']);
            }
        }else{
            $rs = ReturnedMoney::where("id",input('get.id'))->find();
            View::assign('rs',$rs);
            return view();
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}