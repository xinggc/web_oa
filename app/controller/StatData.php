<?php
namespace app\controller;
use think\facade\Db;
use app\model\Config;
use think\facade\View;
use app\model\Menu;
use app\model\UserQywx;
use app\model\UserRole;
use app\model\UserLog;
use app\model\User;
use app\model\Role;
use app\model\Items;
use app\model\Recharge;
use app\model\AccRefund;
use app\model\ReturnedMoney;
use app\model\Customer;
use app\model\ItemConsume;
use app\model\OpAccount;
use app\model\Order;
use think\facade\Request;
use app\model\OperateYeji;
class StatData {
    public function index(){
        $this->get_welcome_info();
        $this->get_total_clicks();
        return view();

    }
    /*
     * 首页
     *   */
    public function get_welcome_info(){
        //客户打款
        $w1[] = ['type','=',1];
        $w1[] = ['status','=',1];
        $arr['amount'] = Recharge::where($w1)->whereMonth('create_time')->sum('amount');
        $arr['amount'] = number_format(floatval($arr['amount']), 2);
        $arr['today_amount'] = Recharge::where($w1)->whereDay('create_time')->sum('amount');
        $arr['today_amount'] = number_format(floatval($arr['today_amount']), 2);
        //客户退款
        $w2[] = ['type','=',3];
        $w2[] = ['status','=',1];
        $arr['return_amount'] = Recharge::where($w2)->whereMonth('create_time')->sum('amount');
        $arr['return_amount'] = number_format(floatval($arr['return_amount']), 2);
        $arr['today_return_amount'] = Recharge::where($w2)->whereDay('create_time')->sum('amount');
        $arr['today_return_amount'] = number_format(floatval($arr['today_return_amount']), 2);
        //账户充值
        $w3[] = ['type','=',2];
        $w3[] = ['status','=',1];
        $arr['currency'] = Recharge::where($w3)->whereMonth('create_time')->sum('practical_currency');
        $arr['currency'] = number_format(floatval($arr['currency']), 3);
        $arr['today_currency'] = Recharge::where($w3)->whereDay('create_time')->sum('practical_currency');
        $arr['today_currency'] = number_format(floatval($arr['today_currency']), 3);
        //账户退款
        $w4[] = ['status','=',2];
        $arr['return_currency'] = AccRefund::where($w4)->whereMonth('create_time')->sum('r_currency');
        $arr['return_currency'] = number_format(floatval($arr['return_currency']), 3);
        $arr['today_return_currency'] = AccRefund::where($w4)->whereDay('create_time')->sum('r_currency');
        $arr['today_return_currency'] = number_format(floatval($arr['today_return_currency']), 3);
        
        //客户数量
        $arr['customer'] = Customer::count('id');
        $arr['month_customer'] = Customer::whereMonth('create_time')->count('id');
        $arr['today_customer'] = Customer::whereDay('create_time')->count('id');
        
        //账户消耗
        $arr['consume'] = ItemConsume::whereMonth('time')->sum('currency');
        $arr['consume'] = number_format(floatval($arr['consume']), 3);
        $arr['today_consume'] = ItemConsume::whereDay('time')->sum('currency');
        $arr['today_consume'] = number_format(floatval($arr['today_consume']), 3);
        
        View::assign('info',$arr);
    }

    //开户进度玫瑰图表数据
    public function account_jindu(){
        if(request()->isPost() && input('t')=='json'){
            //所有账户的数量
            $count = OpAccount::count('id');
            //开户中的数量
            $w1[] =['jindu','in','1,6'];
            $w2[] = ['status','=',1];
            $arr[] = OpAccount::where($w1)->count('id');
            //过素材阶段一的数量
            $w2[] =['jindu','=',3];
            $w2[] = ['status','=',1];
            $arr[] = OpAccount::where($w2)->count('id');
            //过素材阶段二和 过素材 的数量
            $w3[] =['jindu','in','2,4'];
            $w3[] = ['status','=',1];
            $arr[] = OpAccount::where($w3)->count('id');
            //备户完成未投递 的数量
            $w[] = ['is_deliver','=',1];
            $w[] = ['is_finish','=',0];
            $ids = Items::where($w)->column('op_account_id');
            $w4[] =['jindu','=',5];
            $w4[] =['id','not in',$ids];
            $w4[] = ['status','=',1];
            $arr[] = OpAccount::where($w4)->count('id');
            //禁用账户
            $w5[] = ['is_banned','=',0];
            $arr[] = OpAccount::where($w5)->count('id');
            //$arr[] = 10;
            //投递中的数量
            $arr[] = count($ids);
            return json(['msg' => '', 'code' => 1,'data'=>$arr]);
        }else{
            return json(['msg' => '没有数据', 'code' => 0,'data'=>[]]);
        }
           
    }
    //总消耗 （年:1，月:2，日:3）item_consume
    function get_consume_info(){
        if(request()->isPost() && input('t')=='json'){
            $t = input('post.time',3);
            if($t == 1){
               
            }elseif ($t == 2){
                $m = $this->get_month();
                for ($i=0;$i<count($m);$i++){
                    $arr['x'][] = $m[$i];
                    $w[] = ['id','>',0];
                    $arr['y'][] = ItemConsume::where($w)->whereMonth('time', $m[$i]) ->sum('currency');
                }
                $arr['text'] = '最近12个月消耗';
                return json(['msg' => '', 'code' => 1,'data'=>$arr]);
            }elseif ($t == 3){
                $tt = $this->get_time();
                for ($i=0;$i<count($tt);$i++){
                    $arr['x'][] = date("m-d",$tt[$i]);
                    $arr['y'][] = $this->get_day_consume($tt[$i]);
                }
                $arr['text'] = '最近30天消耗';
                return json(['msg' => '', 'code' => 1,'data'=>$arr]);
            }
            
        }
    }

    //获取最近30天时间
    function get_time(){
        $t1 = strtotime(date("Y-m-d",strtotime("-30 day"))) - 86400;
        for ($i=0;$i<30;$i++){
            $t[] = $t1 += 86400;
        }
        return $t;
    }
    //获取最近12个时间
    function get_month(){
         for ($i=11;$i>=0;$i--){
            $t[] = date("Y-m",strtotime("-$i month"));
         }
        return $t;
    }
    //获取每天的消耗
    function get_day_consume($t){
        $s = $t.','.($t+86400);
        //$w[] = ['time','between',$s];
        $w[] = ['time','=',$t];
        $n = ItemConsume::where($w)->sum('currency');
        return $n;
    }

    //消耗排行榜
    public function consume_top(){
        if(request()->isPost() && input('t')=='json'){
            $type = input('post.type',1);
            if($type == 1){
                //获取左天的消耗
                $t = strtotime(date("Y-m-d",strtotime("-1 day")));
                $t2 = $t+86399;
                $arr['text'] = '昨天消耗排行';
            }elseif ($type == 2){
                $w=date('w');//获取当前周的第几天 周日是 0 周一到周六是1-6
                $beginLastweek=strtotime('-'.($w ? $w-1 : 6).' day');//获取本周开始日期，如果$w是0是周日:-6天;其它:$w-1天
                $cur_monday=date('Y-m-d',$beginLastweek);
                $s=date('Y-m-d',strtotime("$cur_monday -7 day"));
                $e=date('Y-m-d',strtotime("$s +6 days"));
                $t = strtotime($s);
                $t2 = strtotime($e)+86399;
                $arr['text'] = '上周消耗排行';
            }elseif ($type == 3){
                $t = mktime(0, 0 , 0,date("m")-1,1,date("Y"));
                $t2 = mktime(23,59,59,date("m") ,0,date("Y"));
                $arr['text'] = '上月消耗排行';
            }elseif($type == 4){
                //自定义时间消耗排行
                $time = input('post.times');
                if($time == 0){
                    $t = strtotime(date("Y-m-d",strtotime("-1 day")));
                    $t2 = $t+86399;
                    $arr['text'] = '昨天消耗排行';
                }else{
                    $arr = explode('&', $time);
                    $t = strtotime($arr[0]);
                    $t2 = strtotime($arr[1])+86399;
                    $arr['text'] = $time.'消耗排行';
                }
                
            }
            $wh[] = ['role_id','=',18];
            $wh[] = ['status','=',1];
            $list = User::field('id,username')->where($wh)->select();
            foreach ($list as $k=>$v){
                $name = preg_replace('/牧唐-运营助理-/','',$v['username']);
                if(strlen($name)>9){
                    $name = preg_replace('/牧唐-运营-/','',$v['username']);
                }
                $arr['x'][] = $name;
                $arr['y'][] = $this->get_run_consume($v['id'],$t,$t2);
            }
            
            
            return json(['msg' => '', 'code' => 1,'data'=>$arr]);
        }
        
        
    }

    //获取需要排名的运营同学
    function get_run_consume($id,$t1,$t2){
        $s = $t1.','.$t2;
        $w[] = ['time','between',$s];
        //$w[] = ['time','=',$t];
        $w[] = ['operation_id','=',$id];
        $n = ItemConsume::where($w)->sum('currency');
        return $n;
    }


    //月度数据
    public function yuestat(OperateYeji $OperateYeji){
        
        return view();
    }
    //获取个人任务
    public function get_rw(OperateYeji $OperateYeji){
        if(request()->isPost() && input('t')=='json'){
            $arr = $OperateYeji->get_gr_rw(input('post.'));

            return json(['code' => 0, 'msg' => '', 'count' => 0, 'data' => $arr['list']]);
        }
    }
    //获取小组数据
    public function get_xz_info(OperateYeji $OperateYeji){
        if(request()->isPost() && input('t')=='json'){
            $list = $OperateYeji->get_xz_infos(input('post.'));
            return json(['code' => 0, 'msg' => '', 'count' => 0, 'data' => $list]);
        }
    }
    //获取运营个人数据
    public function get_gr_info(OperateYeji $OperateYeji){
        if(request()->isPost() && input('t')=='json'){
            $list = $OperateYeji->get_gr_infos(input('post.'));
            return json(['code' => 0, 'msg' => '', 'count' => 0, 'data' => $list]);
        }
    }
    // 获取消耗参考
    public function get_xh_ck(OperateYeji $OperateYeji){
        if(request()->isPost() && input('t')=='json'){
            $rs = $OperateYeji->get_xh_ck_info(input('post.'));
            if($rs){
                return json(['code' => 1, 'msg' => '', 'data' => $rs]);
            }else{
                return json(['code' => 1, 'msg' => '获取失败']);
            }
            
        }
    }



    //获取第三方，每月点击数
    public function get_total_clicks(){
        
        if(request()->isPost() && input('t')=='json'){
            $arr['text'] = '每月点击数';
            $times = input('post.times');
            
            if(empty($times)){
                $t1 = date("Y-m-01",time());
                $t2 = date("Y-m-d",strtotime("$t1 +1 month -1 day"));
            }else{
                $arr = explode('&', $times);
                $t1 = $arr[0];
                $t2 = $arr[1]; 
            }
            
            $rs = file_get_contents('http://tj.mutangtech.com/api/Tj/total_clicks?t1='.$t1.'&t2='.$t2);
            $rs = json_decode($rs,true);
            if($rs['code'] == 0){
                foreach($rs['data'] as $k => $v){
                    $arr['x'][] = $v['name'];
                    $arr['y'][] = $v['num'];
                }
            }else{
                $arr['text'] = $rs['data'];
            }
            
            return json(['msg' => '', 'code' => 1,'data'=>$arr]);

        }
        
    }









}