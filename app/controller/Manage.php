<?php
declare (strict_types=1);

namespace app\controller;

use app\model\Bom;
use app\model\Bomsort;
use app\model\Dept;
use app\model\Grade;
use app\model\Role;
use app\model\User;
use think\console\command\make\Model;
use think\facade\Db;
use think\Request;
use think\response\Json;
use think\facade\View;
use app\model\OpAccount;
use app\model\ViewItemInfo;

class Manage
{
    //行政管理
    /*
     *成员管理
     */
    //跳转页面视图模板
    public function users()
    {
        if (input('t') == 'json') {
            $pn = input('page/d', 1);
            $ps = input('limit/d', 15);
            $username = input('post.username');//接受前台参数赋值
            $department = input('post.department/d');//接受前台参数赋值

            if (!empty($username)) { //判断接受参数不为空
                $where[] = ['username', 'like', "%" . $username . "%"]; //模糊查询
            }
            if (!empty($department)) { //判断接受参数不为空
                $where[] = ['department', 'like', "%" . $department . "%"]; //模糊查询
            }
            if (empty($username) && empty($department)) {//都为空
                $where["status"] = 1; //给与公共参数值
            }
            //降序查询全部数据，如果有输入查询参数则根据参数查询
            $count = User::where($where)->with(['grade', 'dept', 'role'])->count('id');
            $list = User::where($where)->with(['grade', 'dept', 'role'])->page($pn, $ps)->select();
            //返回JSON数据串
            return json(['code' => 0, 'msg' => '', 'count' => $count, 'data' => $list]);
        }
        return view();
    }

    public function accounts()
    {
        echo '<h1 style="text-align: center; line-height: 17;">开发中，敬请期待</h1>';
        // return view('', ['accounts' => config('accounts')]);
    }

    public function manageup()
    {
        echo '<h1 style="text-align: center; line-height: 17;">开发中，敬请期待</h1>';
        // return view('', ['manageup' => config('manageup')]);
    }

    public function bom()
    {
        echo '<h1 style="text-align: center; line-height: 17;">开发中，敬请期待</h1>';
        // return view('', ['bom' => config('bom')]);

    }

    //用户列表
    public function countselect()
    {
        if (request()->isPost()) {//判断是否是post访问
            $username = input('post.username');//接受前台参数赋值
            $department = input('post.department');//接受前台参数赋值

            if (!empty($username)) {//判断接受参数不为空
                $wh[] = ['username', 'like', "%" . $username . "%"];//模糊查询
            }
            if (!empty($department)) {//判断接受参数不为空
                $wh[] = ['department', 'like', "%" . $department . "%"];//模糊查询
            }
            if (empty($username) && empty($department)) {//都为空
                $wh["status"] = 1;//给与公共参数值
            }
            //降序查询全部数据，如果有输入查询参数则根据参数查询
            $list = User::where($wh)->
            with(['grade', 'dept', 'role'])->
            select()->
            toArray();
            //返回JSON数据串
            return json(['code' => 0, 'msg' => '', 'count' => count($list), 'data' => $list]);
        }
    }

    //修改用户信息
    public function user_save()
    {
        $data = input('post.');
        $data['join_time'] = strtotime($data['join_time']);
        $data['birthday'] = strtotime($data['birthday']);
        $num = User::where('id', $data['id'])->save($data);
        if ($num != 0) {
            return json(['code' => 1, 'msg' => '提交成功']);
        } else {
            return 0;
        }
    }

    /**
     * 删除用户
     */
    public function user_del()
    {   
        if(request()->isPost() && input('t')=='json'){
            $id = input('post.id');
            if (empty($id))
                return json(['msg' => '未找到用户', 'code' => 1]);
            
            try {
                User::destroy($id);
                return json(['msg' => '删除成功', 'code' => 0]);
            } catch (\Throwable $th) {
                return json(['msg' => '删除失败', 'code' => 1]);
            }
        }
        
    }


    //查询数据并跳转
    public function user_edit()
    {
        $id = input('id', 0);//获取前台用户ID
        $data = User::with(['grade', 'dept', 'role'])->find($id);
        $data['join_time'] = date('Y-m-d', $data['join_time']?$data['join_time']:0 );
        $data['birthday'] = date('Y-m-d', $data['birthday']?$data['birthday']:0 );
        if (input('t') == 'json')
            return json($data);
        //根据ID获取查询对象
        $datarole = Role::select();
        $datadept = Dept::select();
        $datagrade = Grade::select();
        return view('', ['data' => $data, 'datarole' => $datarole, 'datadept' => $datadept, 'datagrade' => $datagrade]);//返回视图跳转并传值
    }

    //物品管理(分页查询物品数据)
    public function bomselect()
    {
        if (input('t') == 'json') {
            $pn = input('page/d', 1);
            $ps = input('limit/d', 15);
            $username = input('post.bomname');//接受前台参数赋值

            if (!empty($username)) { //判断接受参数不为空
                $where[] = ['username', 'like', "%" . $username . "%"]; //模糊查询
            }
            if (empty($username) && empty($department)) {//都为空
                $where["status"] = 1; //给与公共参数值
            }
            //降序查询全部数据，如果有输入查询参数则根据参数查询
            $count = Bom::where($where)->count();
            $list = Bom::where($where)->with(['sort'])->page($pn, $ps)->select();
            //返回JSON数据串
            return json(['code' => 0, 'msg' => '', 'count' => $count, 'data' => $list]);
        }
        return view();
    }

    //物品管理(查询数据并跳转修页面)
    public function bomedit()
    {
        $bomid = input('bomid', 0);//获取前台物品ID
        $data = Bom::where('bomid', $bomid)->find();//获取查询对象
        $datesort = Bomsort::select();
        return view('bomedit', ['data' => $data, 'datesort' => $datesort]);//返回视图跳转并传值
    }

    //修改物品
    public function bom_save()
    {
        $data = input('post.');
        $time = date('Y-m-d H:i:s', time());
        $data['bomupdatetime'] = strtotime($time);
        $num = Bom::where('bomid', $data['bomid'])->save($data);
        if ($num != 0) {
            return json(['code' => 1, 'msg' => '提交成功']);
        } else {
            return 0;
        }
    }

    //删除物品信息
    public function bom_del()
    {
        $id = input('bomid', 0);
        if (empty($id))
            return json(['msg' => '未找到物品信息', 'code' => 1]);
        if (Bom::where('bomid', $id)->destroy())
            return json(['msg' => '删除成功', 'code' => 0]);
        else return json(['msg' => '删除失败', 'code' => 1]);
    }
    
    //客户运营
    public function client_operation(){
        $wh[] = ['dept_id','=',2];
        $yun = User::where($wh)->field('id,username')->select()->toArray();
        foreach ($yun as $k=>$v){
            $yun[$k]['client'] = $this->get_customer_name($v['id']);
            
        }
        View::assign('list',$yun);
        return view();
    }
    
    function get_customer_name($id){
        $w[] = ['operate_id' ,'=',$id];
        $op_ids = OpAccount::where($w)->column('id');
        if(!empty($op_ids)){
            $w2[] = ['status','=',1];
            $w2[] = ['op_account_id','in',$op_ids];
            return ViewItemInfo::where($w2)->field('customer_name')->select();
        }else{
            return '';
        }
        
    }
  
    // 基础设置
    public function seting(){
        if(request()->isPost() && input('t')=='json'){
            $data = input('post.');
            
            try {
                if(!empty($data['company_wishes'])){
                    C('company_wishes',$data['company_wishes']);
                }
                if(!empty($data['company_ana'])){
                    C('company_ana',$data['company_ana']);
                }
                
                return json(['msg' => '提交成功', 'code' => 1]);
            } catch (\Throwable $th) {
                return json(['msg' => $th->getMessage(), 'code' => 0]);
            }
        }else{
           
            $arr['company_wishes'] = C('company_wishes');
            $arr['company_ana'] = C('company_ana');
            View::assign('arr',$arr);
            return view();
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
