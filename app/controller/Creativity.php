<?php
namespace app\controller;
use think\facade\View;
use think\facade\Db;
use app\model\Copywriter;
class Creativity{
    public function index(){


    }
    // 文案创意
    public function copywriter(){ 
        $Copywriter = new Copywriter();
        if(request()->isPost() && input('t') == 'json'){
            $arr = $Copywriter->list(input('post.'));
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }else{
            $author = Db::name('copywriter_author')->select();
            $label = Db::name('copywriter_label')->select();
            View::assign('author',$author);
            View::assign('label',$label);
            return view();
        }

    }
    // 添加文案
    public function add_copywriter(){
        if(request()->isPost() && input('t')=='json'){
            $Copywriter = new Copywriter();
            $r = $Copywriter->add(input('post.'));
            if($r === true){

                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$r]);
            }
        }else{
            $author = Db::name('copywriter_author')->select();
            $label = Db::name('copywriter_label')->select();
            View::assign('author',$author);
            View::assign('label',$label);
            return view();
        }
    }

    //获取标签
    public function get_label(){
        if(request()->isPost() && input('t')=='json'){
            $label = Db::name('copywriter_label')->field('id value, name')->select();
            return json(['code'=>1,'msg'=>'','data'=>$label]);
        }
    }

    //详情
    public function info_copywriter(){
        $id = input('id');
        $rs = Copywriter::find($id);
        View::assign('rs',$rs);
        return view();
    }

    // 修改文案信息
    public function save_copywriter(){
        $Copywriter = new Copywriter();
        if(request()->isPost() && input('t')=='json'){
            $r = $Copywriter->get_save(input('post.'));
            if($r === true){

                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$r]);
            }
        }else{
            $wh[] = ['id','=',input('id')];
            $rs = $Copywriter->where($wh)->find();
            $author = Db::name('copywriter_author')->select();
            View::assign('rs',$rs);
            View::assign('author',$author);
            return VIew();
        }
    }

    // 添加作者或标签
    public function add_author_label(){
        if(request()->isPost() && input('t')=='json'){
            $d = input('post.');
            try {
                if($d['type'] == 1){
                    Db::name("copywriter_author")->save(['name'=>$d['name']]);
                }elseif($d['type'] == 2){
                    Db::name("copywriter_label")->save(['name'=>$d['name']]);
                }
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (\Throwable $th) {
                //throw $th;
                return json(['code'=>1,'msg'=>$th->getMessage()]);
            }
            
        }
    }





















}