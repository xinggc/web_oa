<?php
namespace app\controller;
use think\Request;
use think\facade\Db;
use think\facade\View;
use app\model\OpAccount;
use app\model\Recharge;
use app\model\Channel;
use app\model\Customer;
use app\model\AccRefund;
use app\model\ViewAccRefund;
use think\Model;
use app\model\User;
use app\model\AccTransfer;
class Refund{
    
    //账户列表he 客户列表
    public function refund_acc(){
        $OpAccount = new OpAccount();
        $acc_list = $OpAccount->field("id,name_id")->select();
        $cu_list = Customer::field("id,name")->select();
        
        View::assign('acc_list',$acc_list);
        View::assign('cu_list',$cu_list);
        return view();
    }
    
    //账户退款列表
    public function list_acc_refund(Recharge $Recharge){
        $ViewAccRefund = new ViewAccRefund(); 
        if(request()->isPost()&&input('t')=='json'){
            //$arr = $ViewAccRefund->get_refund_list(input('post.'));
            $arr = $Recharge->get_acc_succeed_list(input('post.'));
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]); 
        }
    }
    //添加账户退款
    public function add_acc_refund(Recharge $Recharge){
        if(request()->isPost()&&input('t')=='json'){
            $rs = $Recharge->add_acc_refund(input('post.'));
            if($rs === 1){
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
        }
        $w1[] = ['role_id','in','19,14'];
        $user_list = User::field('id,username')->where($w1)->select();
        $w2[] = ['status','=',1];
        $w2[] = ['is_banned','=',1];
        $acc_list = OpAccount::field('id,name_id')->where($w2)->select();
        $num = get_numbers('ATK');

        View::assign('user_list',$user_list);
        View::assign('acc_list',$acc_list);
        View::assign('num',$num);
        return view();
    }
    // 获取账户最近一次充值或转账的点位
    public function get_acc_dianwei(){
        if(request()->isPost()&&input('t')=='json'){
            $w[] = ['r.op_account_id','=',input('post.id')];
            $w[] = ['r.status','=',1];
            $w[] = ['r.type','=',2];
            $rs = Recharge::alias('r')
            ->field('r.*,ch.name channel_name,cu.name customer_name')
            ->join('channel ch','ch.id = r.channel_id','LEFT')
            ->join('customer cu','cu.id = r.customer_id','LEFT')
            ->where($w)->order('r.create_time desc')->find();
            
            $w2[] = ['t.in_acc','=',input('post.id')];
            $w2[] = ['t.status','=',2];
            $rs2 = AccTransfer::alias('t')
            ->field('t.*,ch.name channel_name,cu.name customer_name')
            ->join('channel ch','ch.id = t.channel_id','LEFT')
            ->join('customer cu','cu.id = t.customer_id','LEFT')
            ->where($w2)->order('t.create_time desc')->find();
            
            if($rs['create_time'] < $rs2['create_time']){
                $rs = $rs2;
            }
            
            if($rs){
                return json(['code'=>1,'msg'=>'提交成功','data'=>$rs]);
            }else{
                return json(['code'=>0,'msg'=>'账户异常']);
            }
        }
    }
   
    //修改账户退款
    public function save_acc_refund(Recharge $Recharge){
        $AccRefund = new AccRefund();
        if(request()->isPost()&&input('t')=='json'){
            $data = input('post.');
            $rs = $Recharge->get_save_acc_refund($data);
            
            if($rs === 1){
                log_add(2,'修改账户退款操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            }else{
                return json(['code'=>0,'msg'=>$rs]);
            }
            
        }else{
            $id = input('get.id');
            $rs =Db::name('recharge')->where('id',$id)->find();
            $w1[] = ['role_id','in','19,14'];
            $user_list = User::field('id,username')->where($w1)->select();
            
            View::assign('user_list',$user_list);
            View::assign("rs",$rs);
            return view();
        }
    }
    //删除账户退款
    public function del_acc_refund(){
        if(request()->isPost() && input('t') == 'json'){
            $id = input('post.id');
            try {
                Recharge::destroy($id);
                log_add(3,'删除账户退款操作');
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (\Throwable $th) {
                return json(['code'=>0,'msg'=>$th->getMessage()]);
            }
            
        }
    }
    
    //根据账户id 获取客户信息，账户币余额，最近一次充值的返点和服务费
    public function get_acc_re_cu_info(){
        $Recharge = new Recharge();
        $OpAccount = new OpAccount();
        $Channel = new Channel();
        $Customer = new Customer();
        if(request()->isPost()&&input('t')=='json'){
            $acc_id = input('post.id');
            $wh["op_account_id"] = $acc_id;
            $wh["type"] = 2;
            $arr['re'] = $Recharge->field('rebate,service_pay')->where($wh)->order('create_time desc')->find();
            $arr['balance'] = $OpAccount->field('balance')->find($acc_id);
            $arr['cus'] = $Customer->get_acc_cu_info($acc_id);
            if(empty($arr['cus'])){
                return json(['code'=>0,'msg'=>'当前账户未绑定客户']);
            }
            if(empty($arr['re'])){
                return json(['code'=>0,'msg'=>'当前账户没有充值']);
            }
            if(!empty($arr)){
                return json(['code'=>1,'msg'=>'提交成功','data'=>$arr]);
            }else{
                return json(['code'=>0,'msg'=>'当前账户未绑定客户']);
            }
        }
        
    }
    
    //客户退款列表
    public function list_cu_ewfund(){
        $Recharge = new Recharge();
        if(request()->isPost()&&input('t')=='json'){
            $name = input('post.name');
            $arr = $Recharge->get_refunc_list($name,input("post."));
            return json(['code'=>0,'msg'=>'','count'=>$arr['cnt'],'data'=>$arr['list']]);
        }else{
            $cu_list = Customer::field('id,name')->select();
            View::assign('cu_list',$cu_list);
            return view();
        }
    }
    //添加客户退款
    public function add_cu_refund(){
        $Customer = new Customer();
        $Recharge = new Recharge();
        Db::startTrans();
        if(request()->isPost()&&input('t')=='json'){
            $data = input('post.');
            $data['type'] = 3;
            $data['media_id'] = session('uid');
            $data['create_time'] = time();
            $data['update_time'] = time();
            $data['oper_name'] = session('info.username');
            $money = $Customer->where('id',$data['customer_id'])->value('prepare_money');
            if($data['amount'] > $money){
                return json(['code'=>0,'msg'=>'退款金额不能大于客户余额']);
            }
            $rs = $Recharge->get_add_cu_refund($data);
            if($rs !== true){
                return json(['code'=>0,'msg'=>$rs]);
            }
            try {
                if($data['zfb_num']){
                    $data['cardholder'] = '姓名：'.$data['cardholder'].',支付宝号：'.$data['zfb_num'];
                }else{
                    $data['cardholder'] = '银行卡：'.$data['card_number'].',持卡人：'.$data['cardholder'].',开户行地址：'.$data['bank_name'];
                }
                
                Db::name('recharge')->strict(false)->save($data);
                //冻结客户余额
                Db::name('customer')->where('id', $data['customer_id'])->dec('prepare_money', (float)$data['amount'])->update();
                Db::name('customer')->where('id', $data['customer_id'])->inc('frozen_money', (float)$data['amount'])->update();
                // 提交事务
                Db::commit();
                log_add(1,'审核客户退款');
                //发送消息
                get_msg_info(5,13);
                return json(['code'=>1,'msg'=>'提交成功']);
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return json(['code'=>0,'msg'=>$e->getMessage()]);
            }
            
        }else{
            //生成随机退款编号
            $num = get_numbers('TK');
            //获取客户列表
            $cu_list = Customer::field('id,name,prepare_money')->select();
            
            
            View::assign("cu_list",$cu_list);
            View::assign("num",$num);
            return view();
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}