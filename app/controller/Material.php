<?php
declare (strict_types = 1);

namespace app\controller;

use think\Request;

class Material
{
   
    // 素材管理

    /**
     * 视频素材
     */
    public function video()
    {
        
    }

    /**
     * 图片素材
     */
    public function image()
    {
        
    }

    /**
     * 素材标签
     */
    public function tags()
    {
        
    }

    /**
     * 素材分类
     */
    public function type()
    {
        
    }

}
